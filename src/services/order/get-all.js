import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "@custom/config/General";
import axios from "axios";
import {
    getOrderAllSuccess,
} from "../../redux/actions";
import { showAlert, turnOffAlert } from "../../redux/actions";
import { GET_ORDER_ALL } from "@custom/constants/ActionTypes/Order";

async function getAllOrderAsync(filters) {
    try {
        return await axios.post(API_URL + "order/data-table", filters)
    } catch (error) {
        throw error.response.data;
    }
}

function* getAllOrder(data) {
    try {
        data = yield call(getAllOrderAsync, data.filters);
        yield put(getOrderAllSuccess(data.data));
    } catch (error) {
        yield put(showAlert({
            content: extractError(error),
            variant: "danger"
        }));
    }
    yield delay(5000);
    yield put(turnOffAlert());
}

export function* watchGetAllOrder() {
    yield takeLatest(GET_ORDER_ALL, getAllOrder);
}