import axios from "axios";
import {
    call,takeLatest,put
} from "redux-saga/effects";
import { GET_RECHARGE } from "@custom/constants/ActionTypes/Recharge";
import {API_URL} from "@custom/config/General";
import {getRechargeSuccess} from "../../redux/actions";

function getInfoAsync(){
    return axios.get(API_URL+"recharge/get-info").then(res=>{
        return Promise.resolve(res.data);
    }).catch(error=>{
        return Promise.reject(error);
    });
}

function* getInfo(){
    try{
        let data = yield call(getInfoAsync);
        yield put(getRechargeSuccess(data));
    }catch(error){
        //call alert
    }
}

export function* watchGetInfo(){
    yield takeLatest(GET_RECHARGE,getInfo);
}