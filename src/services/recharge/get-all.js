import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL } from "@custom/config/General";
import axios from "axios";
import {
    getRechargeAllSuccess,
} from "../../redux/actions";
import { GET_RECHARGE_ALL } from "@custom/constants/ActionTypes/Recharge";

async function getAllRechargeAsync(filters) {
    try {
        return await axios.post(API_URL + "recharge/data-table", filters)
    } catch (error) {
        throw error.response.data;
    }
}

function* getAllRecharge(data) {
    try {
        data = yield call(getAllRechargeAsync, data.filters);
        yield put(getRechargeAllSuccess(data.data));
    } catch (error) {
        
    }
}

export function* watchGetAllRecharge() {
    yield takeLatest(GET_RECHARGE_ALL, getAllRecharge);
}