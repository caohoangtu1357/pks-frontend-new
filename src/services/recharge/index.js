import { all } from "redux-saga/effects";
import { watchGetInfo } from "./get-info";
import { watchGetAllRecharge } from "./get-all";

export default function* rootRecharge() {
    yield all([
        watchGetInfo(),
        watchGetAllRecharge()
    ])
}