import axios from "axios";
import {
    call,takeLatest,put
} from "redux-saga/effects";
import { LOAD_MORE_POST } from "@custom/constants/ActionTypes/Post";
import { ALERT } from "@custom/constants/ActionTypes/Alert";
import {API_URL, extractError} from "@custom/config/General";
import {loadMorePostSuccess} from "../../redux/actions";

function loadMorePostAsync(filters){
    return axios.post(API_URL+"post/ajax-load-more",filters).then(res=>{
        return Promise.resolve(res.data);
    }).catch(error=>{
        return Promise.reject(error);
    });
}

function* loadMorePost(data){
    try{
        let post = yield call(loadMorePostAsync,data.filters);
        yield put(loadMorePostSuccess(post));
    }catch(error){
        yield put({type:ALERT,info:{content:extractError(error),variant:"danger"}});
    }
}

export function* watchLoadMorePost(){
    yield takeLatest(LOAD_MORE_POST,loadMorePost);
}