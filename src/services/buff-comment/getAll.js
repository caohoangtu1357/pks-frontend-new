import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL } from "@custom/config/General";
import axios from "axios";
import {
    getBuffCommentAllSuccess,
} from "../../redux/actions";
import { GET_BUFF_COMMENT_ALL } from "@custom/constants/ActionTypes/BuffComment";
import { error } from '@custom/config/Alert';

async function getAllBuffCommentAsync(filters) {
    try {
        return await axios.post(API_URL + "buff-comment/data-table", filters)
    } catch (error) {
        throw error.response.data;
    }
}

function* getAllBuffComment(data) {
    try {
        data = yield call(getAllBuffCommentAsync, data.filters);
        yield put(getBuffCommentAllSuccess(data.data));
    } catch (err) {
        error(err)
    }
}

export function* watchGetAllBuffComment() {
    yield takeLatest(GET_BUFF_COMMENT_ALL, getAllBuffComment);
}