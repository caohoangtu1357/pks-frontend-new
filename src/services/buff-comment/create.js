import { takeLatest, call, put } from "redux-saga/effects";
import { API_URL } from "@custom/config/General";
import axios from "axios";
import {
    createBuffCommentSuccess,
} from "../../redux/actions";
import { CREATE_BUFF_COMMENT } from "@custom/constants/ActionTypes/BuffComment";
import {GET_INFO} from "@custom/constants/ActionTypes/User";
import {error,success} from "@custom/config/Alert";

async function createBuffCommentAsync(attributes) {
    try {
        return await axios.post(API_URL + "buff-comment/create", attributes)
    } catch (error) {
        throw error.response.data;
    }
}

function* createBuffComment(data) {
    try {
        yield call(createBuffCommentAsync, data.attributes);
        yield put(createBuffCommentSuccess());
        success("Thanh toán thành công");
        yield put({type:GET_INFO});
    } catch (err) {
        error(err);
    }
}

export function* watchCreateBuffComment() {
    yield takeLatest(CREATE_BUFF_COMMENT, createBuffComment);
}