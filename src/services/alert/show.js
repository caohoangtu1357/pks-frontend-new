import { takeLatest, call, put, delay } from "redux-saga/effects";
import axios from "axios";
import {
    showAlert,
    turnOffAlert
} from "../../redux/actions";
import { ALERT } from "@custom/constants/ActionTypes/Alert";

function* alert(data) {
    yield put(showAlert({
        content: data.info.content,
        variant: data.info.variant
    }));
    yield delay(5000);
    yield put(turnOffAlert());
}

export function* watchAlert() {
    yield takeLatest(ALERT, alert);
}