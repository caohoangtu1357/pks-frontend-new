import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "@custom/config/General";
import axios from "axios";
import {
    getNotifySuccess
} from "../../redux/actions";
import { ALERT } from "@custom/constants/ActionTypes/Alert";
import { LOAD_NOTIFY } from "@custom/constants/ActionTypes/Notify";

async function getAllNotifyAsync() {
    try {
        return await axios.get(API_URL + "notify/ajax-get-all")
    } catch (error) {
        throw error.response.data;
    }
}

function* getAllNotify(data) {
    try {
        data = yield call(getAllNotifyAsync);
        yield put(getNotifySuccess(data.data));
    } catch (error) {
        yield put({type:ALERT,info:{content:extractError(error),variant:"danger"}});
    }
}

export function* watchGetNotify() {
    yield takeLatest(LOAD_NOTIFY, getAllNotify);
}