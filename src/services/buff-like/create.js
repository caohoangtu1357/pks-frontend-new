import { takeLatest, call, put } from "redux-saga/effects";
import { API_URL } from "@custom/config/General";
import axios from "axios";
import {
    createBuffLikeSuccess,
} from "../../redux/actions";
import { CREATE_BUFF_LIKE } from "@custom/constants/ActionTypes/BuffLike";
import {GET_INFO} from "@custom/constants/ActionTypes/User";
import {success,error} from '@custom/config/Alert';

async function createBuffLikeAsync(BuffLikeInfo) {
    try {
        return await axios.post(API_URL + "buff-like/create", BuffLikeInfo)
    } catch (error) {
        throw error.response.data;
    }
}

function* createBuffLike(data) {
    try {
        yield call(createBuffLikeAsync, data.attributes);
        yield put(createBuffLikeSuccess());
        success("Thanh toán thành công");
        yield put({type:GET_INFO});
    } catch (err) {
        error(err);
    }
}

export function* watchCreateBuffLike() {
    yield takeLatest(CREATE_BUFF_LIKE, createBuffLike);
}