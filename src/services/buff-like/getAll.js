import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL } from "@custom/config/General";
import axios from "axios";
import {
    getBuffLikeAllSuccess,
} from "../../redux/actions";
import { GET_BUFF_LIKE_ALL } from "@custom/constants/ActionTypes/BuffLike";
import {error, success} from '@custom/config/Alert';

async function getAllBuffLikeAsync(filters) {
    try {
        return await axios.post(API_URL + "buff-like/data-table", filters)
    } catch (error) {
        throw error.response.data;
    }
}

function* getAllBuffLike(data) {
    try {
        data = yield call(getAllBuffLikeAsync, data.filters);
        yield put(getBuffLikeAllSuccess(data.data));
    } catch (err) {
        error(err);
    }
}

export function* watchGetAllBuffLike() {
    yield takeLatest(GET_BUFF_LIKE_ALL, getAllBuffLike);
}