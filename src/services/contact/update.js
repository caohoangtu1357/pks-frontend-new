import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "@custom/config/General";
import axios from "axios";
import {
    updateContactSuccess
} from "../../redux/actions";
import { UPDATE_CONTACT } from "@custom/constants/ActionTypes/Contact";
import FormData from 'form-data';
import { success, error } from '@custom/config/Alert';
import { history } from '../../redux/store';

async function updateContactAsync(contactInfo, id) {
    try {
        let dataForm = new FormData();
        dataForm.append('contact_id', contactInfo.contact_id);
        dataForm.append('problem', contactInfo.problem);
        dataForm.append('title', contactInfo.title);
        dataForm.append('description', contactInfo.description);
        if (contactInfo.image) {
            dataForm.append('image', contactInfo.image);
        }
        return await axios.post(API_URL + "contact/update/" + id, dataForm)
    } catch (error) {
        throw error.response.data;
    }
}

function* updateContact(data) {
    try {
        yield call(updateContactAsync, data.contactInfo, data.id);
        yield put(updateContactSuccess());
        success("Cập nhật liên hệ hỗ trợ thành công");
        history.push("/system/contact");
    } catch (err) {
        error(err);
    }
}

export function* watchUpdateContact() {
    yield takeLatest(UPDATE_CONTACT, updateContact);
}