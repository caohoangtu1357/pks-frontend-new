import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "@custom/config/General";
import axios from "axios";
import {
    createContactSuccess,
} from "../../redux/actions";
import { CREATE_CONTACT } from "@custom/constants/ActionTypes/Contact";
import FormData from 'form-data'
import { error, success } from '@custom/config/Alert';
import {history} from '../../redux/store';

async function createContactAsync(contactInfo) {
    try {
        let dataForm = new FormData();
        dataForm.append('contact_id', contactInfo.contact_id);
        dataForm.append('problem', contactInfo.problem);
        dataForm.append('title', contactInfo.title);
        dataForm.append('description', contactInfo.description);
        dataForm.append('image', contactInfo.image);

        return await axios.post(API_URL + "contact/create", dataForm)
    } catch (error) {
        throw error.response.data;
    }
}

function* createContact(data) {
    try {
        yield call(createContactAsync, data.contactInfo);
        yield put(createContactSuccess());
        success("Tạo liên hệ hỗ trợ thành công")
        history.push("/system/contact");
    } catch (err) {
        error(err);
    }
}

export function* watchCreateContact() {
    yield takeLatest(CREATE_CONTACT, createContact);
}