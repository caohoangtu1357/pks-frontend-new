import {all} from "redux-saga/effects";
import { watchCreateContact } from "./create";
import { watchGetAllContact } from "./getAll";
import { watchDeleteContact } from "./delete";
import { watchGetDetailContact } from "./detail";
import { watchUpdateContact } from "./update";
import { watchStatisticContact } from "./statistic";
export default function* rootContact(){
    yield all([
        watchCreateContact(),
        watchGetAllContact(),
        watchDeleteContact(),
        watchGetDetailContact(),
        watchUpdateContact(),
        watchStatisticContact()
    ])
}