import { takeLatest, call, put } from "redux-saga/effects";
import { API_URL, extractError } from "@custom/config/General";
import axios from "axios";
import {
    statiscticContactSuccess,
} from "../../redux/actions";
import { showAlert } from "../../redux/actions";
import { STATISTIC_CONTACT } from "@custom/constants/ActionTypes/Contact";

async function getStatisticAsync() {
    try {
        return await axios.get(API_URL + "contact/total-contact-each-status")
    } catch (error) {
        throw error.response.data;
    }
}

function* getStatisticContact() {
    try {
        var data = yield call(getStatisticAsync);
        yield put(statiscticContactSuccess(data.data));
    } catch (error) {
        yield put(showAlert({
            content: extractError(error),
            variant: "danger"
        }));
    }
}

export function* watchStatisticContact() {
    yield takeLatest(STATISTIC_CONTACT, getStatisticContact);
}