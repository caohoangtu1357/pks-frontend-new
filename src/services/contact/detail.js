import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "@custom/config/General";
import axios from "axios";
import {
    getDetailContactSuccess,
} from "../../redux/actions";
import { showAlert, turnOffAlert } from "../../redux/actions";
import { GET_CONTACT_DETAIL } from "@custom/constants/ActionTypes/Contact";

async function getDetailContactAsync(id) {
    try {
        return await axios.get(API_URL + "contact/detail/"+id)
    } catch (error) {
        throw error.response.data;
    }
}

function* getDetailContact(id) {
    try {
        var data = yield call(getDetailContactAsync, id.id);
        yield put(getDetailContactSuccess(data.data));
    } catch (error) {
        yield put(showAlert({
            content: extractError(error),
            variant: "danger"
        }));
    }
    yield delay(5000);
    yield put(turnOffAlert());
}

export function* watchGetDetailContact() {
    yield takeLatest(GET_CONTACT_DETAIL, getDetailContact);
}