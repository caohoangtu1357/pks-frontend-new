import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "@custom/config/General";
import axios from "axios";
import {
    deleteContactSuccess,
} from "../../redux/actions";
import { DELETE_CONTACT, STATISTIC_CONTACT } from "@custom/constants/ActionTypes/Contact";
import { success, error } from '@custom/config/Alert';

async function deleteContactAsync(id) {
    try {
        return await axios.post(API_URL + "contact/deleted", { id: id })
    } catch (error) {
        throw error.response.data;
    }
}

function* deleteContact(data) {
    try {
        yield call(deleteContactAsync, data.id);
        yield put(deleteContactSuccess());
        success("Xóa liên hệ hỗ trợ thành công");
        yield put({ type: STATISTIC_CONTACT });
    } catch (err) {
        error(err);
    }
}

export function* watchDeleteContact() {
    yield takeLatest(DELETE_CONTACT, deleteContact);
}