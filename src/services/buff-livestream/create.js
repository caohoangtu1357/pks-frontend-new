import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL } from "@custom/config/General";
import axios from "axios";
import {
    createBuffLiveStreamSuccess,
} from "../../redux/actions";
import { CREATE_BUFF_LIVE_STREAM } from "@custom/constants/ActionTypes/BuffLiveStream";
import {getInfo} from '../../redux/actions';
import {success,error} from '@custom/config/Alert';

async function createBuffLiveStreamAsync(attributes) {
    try {
        return await axios.post(API_URL + "buff-livestream/create", attributes)
    } catch (error) {
        throw error.response.data;
    }
}

function* createBuffLiveStream(data) {
    try {
        yield call(createBuffLiveStreamAsync, data.attributes);
        yield put(createBuffLiveStreamSuccess());
        success("Thanh toán thành công");
        yield put(getInfo());
    } catch (err) {
        error(err)
    }
}

export function* watchCreateBuffLiveStream() {
    yield takeLatest(CREATE_BUFF_LIVE_STREAM, createBuffLiveStream);
}