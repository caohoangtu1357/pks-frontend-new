import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL } from "@custom/config/General";
import axios from "axios";
import {
    getBuffLiveStreamAllSuccess,
} from "../../redux/actions";
import { GET_BUFF_LIVE_STREAM_ALL } from "@custom/constants/ActionTypes/BuffLiveStream";
import {error} from '@custom/config/Alert';

async function getAllBuffLiveStreamAsync(filters) {
    try {
        return await axios.post(API_URL + "buff-livestream/data-table", filters)
    } catch (error) {
        throw error.response.data;
    }
}

function* getAllBuffLiveStream(data) {
    try {
        data = yield call(getAllBuffLiveStreamAsync, data.filters);
        yield put(getBuffLiveStreamAllSuccess(data.data));
    } catch (err) {
        error(err);
    }
}

export function* watchGetAllBuffLiveStream() {
    yield takeLatest(GET_BUFF_LIVE_STREAM_ALL, getAllBuffLiveStream);
}