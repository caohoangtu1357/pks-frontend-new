import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "@custom/config/General";
import axios from "axios";
import {
    getBuffShareAllSuccess,
} from "../../redux/actions";
import { showAlert, turnOffAlert } from "../../redux/actions";
import { GET_BUFF_SHARE_ALL } from "@custom/constants/ActionTypes/BuffShare";
import { error, success } from '@custom/config/Alert';

async function getAllBuffShareAsync(filters) {
    try {
        return await axios.post(API_URL + "buff-share/data-table", filters)
    } catch (error) {
        throw error.response.data;
    }
}

function* getAllBuffShare(data) {
    try {
        data = yield call(getAllBuffShareAsync, data.filters);
        yield put(getBuffShareAllSuccess(data.data));
    } catch (err) {
        error(err);
    }
}

export function* watchGetAllBuffShare() {
    yield takeLatest(GET_BUFF_SHARE_ALL, getAllBuffShare);
}