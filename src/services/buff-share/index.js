import {all} from "redux-saga/effects";
import { watchGetAllBuffShare } from "./getAll";
import {watchCreateBuffShare} from "./create";

export default function* rootBuffShare(){
    yield all([
        watchGetAllBuffShare(),
        watchCreateBuffShare()
    ])
}