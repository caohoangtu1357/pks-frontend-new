import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL } from "@custom/config/General";
import axios from "axios";
import {
    createBuffShareSuccess,
} from "../../redux/actions";
import { CREATE_BUFF_SHARE } from "@custom/constants/ActionTypes/BuffShare";
import { GET_INFO } from "@custom/constants/ActionTypes/User";
import { success, error } from '@custom/config/Alert';

async function createBuffShareAsync(attributes) {
    try {
        return await axios.post(API_URL + "buff-share/create", attributes)
    } catch (error) {
        throw error.response.data;
    }
}

function* createBuffShare(data) {
    try {
        yield call(createBuffShareAsync, data.attributes);
        yield put(createBuffShareSuccess());
        success("Thanh toán thành công");
        yield put({ type: GET_INFO });
    } catch (err) {
        error(err);
    }
}

export function* watchCreateBuffShare() {
    yield takeLatest(CREATE_BUFF_SHARE, createBuffShare);
}