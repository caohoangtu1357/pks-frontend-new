import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "@custom/config/General";
import axios from "axios";
import { UPDATE_FB_ID, GET_INFO } from "@custom/constants/ActionTypes/User";
import {
    updateFbIdSuccess,
} from "../../redux/actions";
import {success,error} from '@custom/config/Alert';

async function updateFbIdAsync(fbID) {
    try {
        return await axios.put(API_URL + "user/update-fb-id", fbID)
    } catch (error) {
        throw error.response.data;
    }
}

function* updateFbId(fbID) {
    try {
        yield call(updateFbIdAsync, fbID.fbId);
        yield put(updateFbIdSuccess());
        yield put({ type: GET_INFO });
        success("Cập nhật FB ID thành công");
    } catch (err) {
        error(err);
    }
}

export function* watchUpdateFbId() {
    yield takeLatest(UPDATE_FB_ID, updateFbId);
}