import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "@custom/config/General";
import axios from "axios";
import { CHANGE_PASSWORD, GET_INFO } from "@custom/constants/ActionTypes/User";
import {
    changePasswordSuccess,
} from "../../redux/actions";

import {success,error} from '@custom/config/Alert';

async function changePasswordAsync(passwordInfo) {
    try {
        return await axios.put(API_URL + "user/change-password", passwordInfo.passwordInfo)
    } catch (error) {
        throw error.response.data;
    }
}

function* changePassword(passwordInfo) {
    try {
        yield call(changePasswordAsync, passwordInfo);
        yield put(changePasswordSuccess());
        yield put({ type: GET_INFO });
        success("Cập nhật mật khẩu thành công");
    } catch (err) {
        error(err);
    }
}

export function* watchChangePassword() {
    yield takeLatest(CHANGE_PASSWORD, changePassword);
}