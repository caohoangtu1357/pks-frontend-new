import {takeLatest,call,put} from "redux-saga/effects";
import {API_URL} from "@custom/config/General";
import axios from "axios";
import {VERIFY_ACCOUNT} from "@custom/constants/ActionTypes/Auth";
import {ALERT} from "@custom/constants/ActionTypes/Alert";
import { extractError } from "@custom/config/General";
import {history} from "../../redux/store";
import { error, success } from '@custom/config/Alert';

async function verifyAccountAsync(data){
    try {
        let res = await axios.post(API_URL + "verify-sms-token", {...data})
        return res.data;
    } catch (error) {
        throw error.response.data;
    }
}

function* verifyAccount(actionData){
    try{
        yield call(verifyAccountAsync,actionData.credentials);
        success("Chúc mừng bạn đã xác thực tài khoản thành công.")
        history.push("/signin");
    }catch(err){
        error(err);
    }
}

export function* watchVerifyAccount(){
    yield takeLatest(VERIFY_ACCOUNT,verifyAccount);
}