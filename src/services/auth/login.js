import {takeLatest,call,put} from "redux-saga/effects";
import {API_URL} from "@custom/config/General";
import axios from "axios";
import {LOGIN} from "@custom/constants/ActionTypes/Auth";
import { GET_INFO } from "@custom/constants/ActionTypes/User";
import {logginSuccess} from "../../redux/actions";
import {error} from '@custom/config/Alert';

async function loginAsync(data){
    try {
        let res = await axios.post(API_URL + "login", {...data})
        return res.data;
    } catch (error) {
        throw error.response.data;
    }
}

function* login(actionData){
    try{
        let data = yield call(loginAsync,actionData.credentials);
        localStorage.setItem("auth-token",data['token_type']+" "+data["token"]);
        yield put(logginSuccess(data));
        yield put({type:GET_INFO});
    }catch(err){
        error(err);
    }
}

export function* watchLogin(){
    yield takeLatest(LOGIN,login);
}