import {takeLatest,call,put} from "redux-saga/effects";
import {API_URL} from "@custom/config/General";
import axios from "axios";
import {VERIFY_RESET_PASSWORD} from "@custom/constants/ActionTypes/Auth";
import {success,error} from '@custom/config/Alert';
import {history} from '../../redux/store'

async function verifyResetPasswordAsync(data){
    try {
        let res = await axios.post(API_URL + "verify-reset-password", {...data})
        return res.data;
    } catch (error) {
        throw error.response.data;
    }
}

function* verifyResetPassword(actionData){
    try{
        yield call(verifyResetPasswordAsync,actionData.credentials);
        success("Đặt lại mật khẩu thành công.");
        history.push("/signin");
    }catch(err){
        error(err);
    }
}

export function* watchVerifyResetPassword(){
    yield takeLatest(VERIFY_RESET_PASSWORD,verifyResetPassword);
}