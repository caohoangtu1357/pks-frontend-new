import {takeLatest,call,put} from "redux-saga/effects";
import {API_URL} from "@custom/config/General";
import axios from "axios";
import {LOGOUT} from "@custom/constants/ActionTypes/Auth";
import { GET_INFO } from "@custom/constants/ActionTypes/User";

function logoutAsync(){
    return axios.post(API_URL+"logout",{
        token: localStorage.getItem("auth-token")
    }).then(res=>{
        let data = res.data;
        return Promise.resolve(data);
    }).catch(error=>{
        return Promise.reject(error);
    });
}

function* logout(){
    try{
        yield call(logoutAsync);
        localStorage.removeItem("auth-token");
        yield put({type:GET_INFO});
    }catch(error){
        //call alert
    }
}

export function* watchLogout(){
    yield takeLatest(LOGOUT,logout);
}