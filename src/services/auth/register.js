import { takeLatest, call, put } from "redux-saga/effects";
import { API_URL } from "@custom/config/General";
import axios from "axios";
import { REGISTER } from "@custom/constants/ActionTypes/Auth";
import { history } from "../../redux/store";
import { error } from '@custom/config/Alert';

async function registerAsync(data) {
    try {
        return await axios.post(API_URL + "register", { ...data })
    } catch (error) {
        throw error.response.data;
    }
}

function* register(data) {
    try {
        yield call(registerAsync, data.credentials);
        history.push("/verify-account");
    } catch (err) {
        error(err);
    }
}

export function* watchRegister() {
    yield takeLatest(REGISTER, register);
}