import { takeLatest, call, put } from "redux-saga/effects";
import { API_URL } from "@custom/config/General";
import axios from "axios";
import { RESET_PASSWORD } from "@custom/constants/ActionTypes/Auth";
import { error } from '@custom/config/Alert';
import { history } from '../../redux/store';

async function resetPasswordAsync(data) {
    try {
        let res = await axios.post(API_URL + "reset-password", { ...data })
        return res.data;
    } catch (error) {
        throw error.response.data;
    }
}

function* resetPassword(actionData) {
    try {
        yield call(resetPasswordAsync, actionData.credentials);
        history.push("/verify-reset-password");
    } catch (err) {
        error(err);
    }
}

export function* watchResetPassword() {
    yield takeLatest(RESET_PASSWORD, resetPassword);
}