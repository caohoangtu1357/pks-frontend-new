import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import DirectionsIcon from '@material-ui/icons/Directions';
import { Cached } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        alignItems: 'center',
        backgroundColor: theme.palette.background.default,
        width: 250,
        marginBottom: 10
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    divider: {
        height: 28,
        margin: 4,
    },
}));

export default function CustomizedInputBase(props) {
    const classes = useStyles();
    const [search, setSearch] = useState();

    return (
        <Paper component="form" className={classes.root}>
            <InputBase
                value={search}
                className={classes.input}
                placeholder="Tìm kiếm id"
                inputProps={{ 'aria-label': 'Tìm kiếm' }}
                onChange={e => setSearch(e.target.value)}
            />
            <IconButton type="button" onClick={() => props.handleSearch(search)} className={classes.iconButton} aria-label="search">
                <SearchIcon />
            </IconButton>
            <IconButton type="button" onClick={() => { props.handleSearch(""); setSearch("") }} className={classes.iconButton} aria-label="reload">
                <Cached />
            </IconButton>
        </Paper>
    );
}