import React from "react";
import { makeStyles } from "@material-ui/styles";
import { RECHARGE_STATUS } from '@custom/constants/Facebook';

const styles = makeStyles({
    labelProcess: {
        backgroundColor: "#2196f3",
        color: "white",
        padding: "5px 15px 5px 15px",
        borderRadius: 5,
        fontSize:14
    },
    labelSuccess: {
        backgroundColor: "#8bc34a",
        color: "white",
        padding: "5px 10px 5px 10px",
        borderRadius: 5,
        fontSize:14
    },
    labelDanger: {
        backgroundColor: "#dc3545",
        color: "white",
        padding: "5px 10px 5px 10px",
        borderRadius: 5,
        fontSize:14
    }
});

export function StatusColumn(props) {
    const classes = styles();
    if (props.status == 0) {
        return <span className={classes.labelProcess}>{RECHARGE_STATUS[0].label}</span>
    }
    if (props.status == 1) {
        return <span className={classes.labelSuccess}>{RECHARGE_STATUS[1].label}</span>
    }
    if (props.status == 2) {
        return <span className={classes.labelDanger}>{RECHARGE_STATUS[1].label}</span>
    }
}