import React from "react";
import { makeStyles } from "@material-ui/styles";
import { BUFF_STATUS } from '@custom/constants/Facebook';

const styles = makeStyles({
    labelWaiting: {
        backgroundColor: "#2196f3",
        color: "white",
        padding: "5px 15px 5px 15px",
        borderRadius: 5,
        fontSize:14
    },
    labelProcessed: {
        backgroundColor: "#8bc34a",
        color: "white",
        padding: "5px 10px 5px 10px",
        borderRadius: 5,
        fontSize:14
    },
});

export function StatusColumn(props) {
    const classes = styles();
    if (props.status == 0) {
        return <span className={classes.labelWaiting}>{BUFF_STATUS[0].label}</span>
    }
    if (props.status == 1) {
        return <span className={classes.labelProcessed}>{BUFF_STATUS[1].label}</span>
    }
}