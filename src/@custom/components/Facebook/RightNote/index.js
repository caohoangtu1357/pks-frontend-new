import React from "react";
import { Alert } from "@material-ui/lab";
import { makeStyles } from "@material-ui/styles";
import "./index.scss";

const useStyles = makeStyles(theme => ({
    infoAlert: {
        "& .MuiAlert-icon": {
            display: "none"
        }
    }
}));

const RightNote = () => {
    const classes = useStyles();
    return (
        <div className="right-info">
            <div className="info-item">
                <Alert className variant="filled" severity="info" style={{ justifyContent: "center" }} className={classes.infoAlert}>
                    Thông tin<br/>
                    - Tất cả loại Like Buff đều sử dụng tài khoản Facebook Việt có Avatar để tăng Like.<br/>
                    - Đối với loại Like Buff Trực Tiếp sẽ có tỉ lệ tụt nếu nick của họ bị Checkpoint.<br/>
                    - Các đơn Buff sẽ không được hoàn tiền với bất kì lý do gì, cân nhắc trước khi Order.
                </Alert>
            </div>
            <div className="info-item">
                <Alert variant="filled" severity="error" style={{ justifyContent: "center" }} className={classes.infoAlert}>
                    Lưu ý<br/>
                    - Ngiêm cấm Buff các ID Seeding có nội dung vi phạm pháp luật, chính trị, đồi trụy... Nếu cố tình buff bạn sẽ bị trừ hết tiền và banned khỏi hệ thống vĩnh viễn, và phải chịu hoàn toàn trách nhiệm trước pháp luật.
                </Alert>
            </div>
        </div>
    )
}

export default RightNote;