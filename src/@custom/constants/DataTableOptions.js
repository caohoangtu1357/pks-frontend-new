import React from 'react';

export default (rows,page=1,sizePerPage=10) => {
    return {
        page: page,
        sizePerPage: sizePerPage,
        sizePerPageList: [ 5, 10, 20 ],
        showTotal: true,
        totalSize: rows.length != 0 ? rows.total : 0,
        hidePageListOnlyOnePage:true,
        paginationTotalRenderer:(from, to, size)=>{
            return(
                <span className="react-bootstrap-table-pagination-total"> Hiển thị từ { from } đến { to } của { size }</span>
            )
        }
    }
    
};