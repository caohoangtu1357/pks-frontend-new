export const TIME_LIVESTREAM = [
    { value: 30, label: "30 phút" },
    { value: 45, label: "45 phút" },
    { value: 60, label: "60 phút" },
    { value: 90, label: "90 phút" },
    { value: 120, label: "120 phút" },
    { value: 150, label: "150 phút" },
    { value: 180, label: "180 phút" },
    { value: 210, label: "210 phút" },
    { value: 240, label: "240 phút" },
];

export const BUFF_STATUS = [
    { value: 0, label: "Đang chờ" },
    { value: 1, label: "Đã gửi" },
];

export const RECHARGE_STATUS = [
    { value: 0, label: "Xử lý" },
    { value: 1, label: "Thành công" },
    { value: 2, label: "Thất bại" }
];