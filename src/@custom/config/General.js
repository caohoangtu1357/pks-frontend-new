import { error, success, info } from '@custom/config/Alert';
const API_URL = process.env.REACT_APP_BASE_URL;
const SERVER_CONTACT_IMAGE = process.env.REACT_APP_SERVER_CONTACT_IMAGE;
const REACT_APP_SERVER_NOTIFY_IMAGE = process.env.REACT_APP_SERVER_NOTIFY_IMAGE;
const REACT_APP_SERVER_POST_IMAGE = process.env.REACT_APP_SERVER_POST_IMAGE;

function extractError(error) {
    if (error.msg) {
        return error.msg;
    }
    var errors = error.errors;
    for (const property in errors) {
        return errors[property];
    }
    return "Lỗi hệ thống.";
}

function getContactImageUrl(imagePath) {
    if (typeof imagePath == "string") {
        return SERVER_CONTACT_IMAGE + imagePath.replace("public", "storage")
    }
    return "";
}

function processIdPost(idPost) {
    try {
        var id = idPost;
        var result = null;
        var post_id = id['match'](/(.*)\/posts\/([0-9]{8,})/);
        var photo_id = id['match'](/(.*)\/photo.php\?fbid=([0-9]{8,})/);
        var photo_id1 = id['match'](/(.*)\/photo\?fbid=([0-9]{8,})/);
        var photo_id2 = id['match'](/(.*)\/photo\/\?fbid=([0-9]{8,})/);
        var video_id = id['match'](/(.*)\/video.php\?v=([0-9]{8,})/);
        var story_id = id['match'](/(.*)\/story.php\?story_fbid=([0-9]{8,})/);
        var link_id = id['match'](/(.*)\/permalink.php\?story_fbid=([0-9]{8,})/);
        var other_id = id['match'](/(.*)\/([0-9]{8,})/);
        var comment_id = id['match'](/(.*)comment_id=([0-9]{8,})/);
        if (post_id) {
            result = post_id[2]
        } else {
            if (photo_id) {
                result = photo_id[2]
            } else {
                if (photo_id1) {
                    result = photo_id1[2]
                } else {
                    if (photo_id2) {
                        result = photo_id2[2]
                    } else {
                        if (video_id) {
                            result = video_id[2]
                        } else {
                            if (story_id) {
                                result = story_id[2]
                            } else {
                                if (link_id) {
                                    result = link_id[2]
                                } else {
                                    if (other_id) {
                                        result = other_id[2]
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };
        if (comment_id) {
            result += '_' + comment_id[2]
        };

        if (result == null) {
            var regex = /^[0-9]+$/;
            if (id.match(regex)) {
                result = id;

            }
            else {
                error("Link bài viết bạn nhập không hợp lệ");
                return "";
            }
        }
        return result;
    } catch (err) {
        error("Link bài viết bạn nhập không hợp lệ");
        return "";
    }
}

function convertNumberToK(number) {
    var result = Math.floor(number / 1000);
    if (result > 0) {
        return result + "K";
    }
    return number;
}

function numberWithDot(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function validatePhone(value) {
    let phone = value.match(/(^(032|033|034|035|036|037|038|039|070|079|077|076|078|083|084|085|081|082|056|058|059|088|091|094|089|090|093|092|099|086|096|097|098|090)[0-9]{7}$)|(^(0162|0163|0164|0165|0166|0167|0168|0169|0120|0121|0122|0126|0128|0911|0941|0123|0124|0125|0127|0129|0188|0186|0993|0994|0996|0199|0995|0997)[0-9]{7}$)/g);
    if(!phone){
        error("Số điện thoại không hợp lệ.");
        return false;
    }
    return true;
};

function validateFacebookProfile(value) {
    let profile = value.match(/^(?:https?:\/\/)?(?:www\.)?facebook.com((\/[a-zA-Z.0-9\-]+)$|(\/profile.php\?id=[0-9]{15})$)/g);
    if(!profile){
        error('Link facebook không hợp lệ.');
        return false;
    }
    return true;
};

function validateEmail(value) {
    let email = value.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,6})+$/);
    if(!email){
        error("Email không hợp lệ.");
        return false;
    }
    return true;
};

function getNotifyImage(imagePath){
    if (typeof imagePath == "string") {
        return REACT_APP_SERVER_NOTIFY_IMAGE + imagePath.replace("public", "storage")
    }
    return "";
}

function getPostImage(imagePath){
    if (typeof imagePath == "string") {
        return REACT_APP_SERVER_POST_IMAGE + imagePath.replace("public", "storage")
    }
    return "";
}

export {
    API_URL,
    SERVER_CONTACT_IMAGE,
    extractError,
    processIdPost,
    convertNumberToK,
    numberWithDot,
    numberWithCommas,
    getContactImageUrl,
    validateEmail,
    validatePhone,
    validateFacebookProfile,
    getNotifyImage,
    getPostImage
}