import Sweetalert2 from 'sweetalert2'
import './style.scss'

function Swal(props){
    
    const customClass={
        container: '',
        popup: '',
        header: '',
        title: '',
        closeButton: '',
        icon: 'swal icon-content',
        image: '',
        content: '',
        htmlContainer: 'swal html-container',
        input: '',
        inputLabel: '',
        validationMessage: '',
        actions: 'swal actions',
        confirmButton: 'swal btn-confirm',
        denyButton: '',
        cancelButton: 'swal btn-cancel',
        loader: '',
        footer: ''
    }

    Sweetalert2.fire({
        title: props.title,
        text: props.text,
        icon: props.icon,
        showCancelButton: true,
        confirmButtonText: "Xác nhận!",
        cancelButtonText: "Không",
        closeOnConfirm: false,
        closeOnCancel: true,
        width:"47rem",
        customClass:customClass
    }).then(result=>{
        if(result.isConfirmed){
            props.hadleAction();
        }
    });
    
}

export default Swal;