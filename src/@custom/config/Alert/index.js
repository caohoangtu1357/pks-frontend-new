import React from 'react';
import { NotificationManager } from 'react-notifications';
import { history } from '../../../redux/store';

export function extractError(error) {
    if (error.msg) {
        return error.msg;
    }
    var errors = error.errors;
    for (const property in errors) {
        return errors[property];
    }
    return "Lỗi hệ thống.";
}

export function success(content, time = 2000) {
    NotificationManager.success(content, "Thông báo", time);
}

export function warning(content, time = 2000) {
    NotificationManager.warning(content, "Thông báo", time);
}

export function error(content, time = 2000) {
    if (typeof content == "string") {
        NotificationManager.error(content, "Thông báo", time);
    } else {
        let str = extractError(content);
        if (str == "Vui lòng đăng nhập lại.") {
            NotificationManager.error("Hết phiên đăng nhập, vui lòng đăng nhập lại.", "Thông báo", time);
            setTimeout(function () {
                history.go();
            }, 3000);
        } else {
            NotificationManager.error(str, "Thông báo", time);
        }
    }

}

export function info(content, time = 2000) {
    NotificationManager.info(content, "Thông báo", time);
}