import React from 'react';
import VerifyResetPass from '../../@jumbo/components/Common/authComponents/VerifyResetPass';

const verifyResetPassword = () => <VerifyResetPass variant="default"/>;

export default verifyResetPassword;