import React, { lazy, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router';
import PageLoader from '../../@jumbo/components/PageComponents/PageLoader';

const Facebook = ({ match }) => {
  const requestedUrl = match.url.replace(/\/$/, '');
  return (
    <Suspense fallback={<PageLoader />}>
      <Switch>
        <Redirect exact from={`${requestedUrl}/`} to={`${requestedUrl}/buff-like`} />
        <Route path={`${requestedUrl}/buff-like`} component={lazy(() => import('./BuffLike'))} />
        <Route path={`${requestedUrl}/buff-comment`} component={lazy(() => import('./BuffComment'))} />
        <Route path={`${requestedUrl}/buff-livestream`} component={lazy(() => import('./BuffLivestream'))} />
        <Route path={`${requestedUrl}/buff-share`} component={lazy(() => import('./BuffShare'))} />
        <Route component={lazy(() => import('../ExtraPages/404'))} />
      </Switch>
    </Suspense>
  );
};

export default Facebook;
