import GridContainer from "@jumbo/components/GridContainer";
import { Grid } from "@material-ui/core";
import React, { useState } from "react";
import { Typography } from "@material-ui/core";
import "./index.scss";
import { Alert } from "@material-ui/lab";
import { makeStyles } from "@material-ui/styles";
import { useDispatch } from "react-redux";
import { createBuffComment } from '../../../../redux/actions';
import { useForm } from "react-hook-form";
import { processIdPost } from '@custom/config/General';

const useStyles = makeStyles({
    infoAlert: {
        "& .MuiAlert-icon": {
            display: "none"
        }
    }
});

const Form = () => {

    const [idPost, setIdPost] = useState("");
    const [number, setNumber] = useState(10);
    const [comments, setComments] = useState("");
    const [note, setNote] = useState("");
    const [total, setTotal] = useState(1000);

    const dispatch = useDispatch();
    const { register, handleSubmit, formState: { errors }, reset } = useForm();

    function handleCreate(data, e) {
        let idPostProcessed = processIdPost(data.idPost);
        if (idPostProcessed == "") {
            return false;
        }

        dispatch(createBuffComment({
            uid: idPostProcessed,
            number: data.number,
            note: data.note,
            comments: data.comments
        }));
    }

    const classes = useStyles();
    return (
        <div className="buff-comment-form">
            <Typography className="heading" variant="h3">Thông tin buff</Typography>
            <form onSubmit={handleSubmit(handleCreate)}>
                <GridContainer>
                    <Grid item sm={3}>
                        <div className="control-label">Link bài viết</div>
                    </Grid>
                    <Grid item sm={7}>
                        <input
                            {...register("idPost", { required: true })}
                            type="text"
                            value={idPost}
                            onChange={e => setIdPost(e.target.value)}
                            className="form-control"
                        />
                        {errors.idPost?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                    </Grid>
                </GridContainer>
                <GridContainer>
                    <Grid item sm={3}>
                        <div className="control-label">Nội dung</div>
                    </Grid>
                    <Grid item sm={7}>
                        <textarea
                            {...register("comments", { required: true })}
                            placeholder="Nội dung"
                            value={comments}
                            onChange={e => setComments(e.target.value)}
                            rows={10}
                            className="form-control"
                        ></textarea>
                        {errors.comments?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                    </Grid>
                </GridContainer>
                <GridContainer>
                    <Grid item sm={3}>
                        <div className="control-label">Số lượng</div>
                    </Grid>
                    <Grid item sm={7}>
                        <input
                            {...register("number", { required: true, min: 1 })}
                            type="number"
                            value={number}
                            onChange={e => { setNumber(e.target.value); setTotal(e.target.value * 100); }}
                            className="form-control"
                        />
                        {errors.number?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                        {errors.number?.type === "min" && <p className="form-validate-error">Số lượng tối thiểu là 1.</p>}
                    </Grid>
                </GridContainer>
                <GridContainer>
                    <Grid item sm={3}>
                        <div className="control-label">Ghi chú</div>
                    </Grid>
                    <Grid item sm={7}>
                        <input
                            {...register("note")}
                            type="text"
                            value={note}
                            onChange={e => setNote(e.target.value)}
                            className="form-control"
                        />
                    </Grid>
                </GridContainer>
                <GridContainer>
                    <Grid item sm={3}>
                    </Grid>
                    <Grid item sm={7}>
                        <Alert variant="filled" severity="info" style={{ justifyContent: "center" }} className={classes.infoAlert}>
                            Tổng: {total} nCoin<br />
                            Giá buff là 100 nCoin/Comment
                        </Alert>
                    </Grid>
                </GridContainer>
                <div className="btn-purchase">
                    <button className="btn btn-success" type="submit">
                        Thanh toán
                    </button>
                </div>
            </form>
        </div>
    )
}

export default Form;