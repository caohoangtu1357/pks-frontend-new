import React from 'react';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { Box, Grid, Typography } from '@material-ui/core';
import { AccountBalance, Add, HistorySharp, ListAlt } from '@material-ui/icons';
import List from './List';
import Form from './Form';
import PageContainer from '@jumbo/components/PageComponents/layouts/PageContainer';
import GridContainer from '@jumbo/components/GridContainer';
import RightNote from '@custom/components/Facebook/RightNote';

const useStyles = makeStyles({
    root: {
        flexGrow: 1,
        padding: "20px 10px"
    },
    infoAlert: {
        "& .MuiAlert-icon": {
            display: "none"
        }
    },
    bankImage: {
        maxHeight: 70
    }
});

function a11yProps(index) {
    return {
        id: `nav-tab-${index}`,
        'aria-controls': `nav-tabpanel-${index}`,
    };
}

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <Box
            role="tabpanel"
            hidden={value !== index}
            id={`nav-tabpanel-${index}`}
            aria-labelledby={`nav-tab-${index}`}
            {...other}>
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </Box>
    );
}

function LinkTab(props) {
    return (
        <Tab
            component="a"
            onClick={event => {
                event.preventDefault();
            }}
            {...props}
        />
    );
}

const breadcrumbs = [
    { label: 'Facebook', link: '#' },
    { label: 'Buff livestream bài viết', link: '/facebook/buff-livestream' },
];

const IconLabelTabs = () => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <PageContainer heading="Buff livestream bài viết" breadcrumbs={breadcrumbs}>
            <GridContainer>
                <Grid item sm={8}>
                    <Paper square className={classes.root} elevation={3}>
                        <Box style={{ display: "flex", justifyContent: "flex-end" }}>
                            <Tabs
                                value={value}
                                onChange={handleChange}
                                variant="fullWidth"
                                indicatorColor="secondary"
                                textColor="secondary"
                                aria-label="icon label tabs example">
                                <LinkTab style={{ maxWidth: 200 }} icon={<Add />} label="Thêm ID Buff" {...a11yProps(0)} />
                                <LinkTab style={{ maxWidth: 200 }} icon={<ListAlt />} label="Danh sách Buff" {...a11yProps(1)} />
                            </Tabs>
                        </Box>
                        <TabPanel value={value} index={0}>
                            <Form />
                        </TabPanel>
                        <TabPanel value={value} index={1}>
                            <List />
                        </TabPanel>
                    </Paper>
                </Grid>
                <Grid item sm={4}>
                    <Paper square className={classes.root} elevation={3}>
                        <RightNote />
                    </Paper>
                </Grid>
            </GridContainer>
        </PageContainer>
    );
}

export default IconLabelTabs;