import GridContainer from "@jumbo/components/GridContainer";
import { Grid } from "@material-ui/core";
import React, { useState } from "react";
import { Typography } from "@material-ui/core";
import "./index.scss";
import { Alert } from "@material-ui/lab";
import { makeStyles } from "@material-ui/styles";
import { useDispatch } from "react-redux";
import { createBuffLivestream } from '../../../../redux/actions';
import { useForm } from "react-hook-form";
import { TIME_LIVESTREAM } from "@custom/constants/Facebook.js";
import { processIdPost } from '@custom/config/General';

const useStyles = makeStyles({
    infoAlert: {
        "& .MuiAlert-icon": {
            display: "none"
        }
    }
});

function Select(props) {

    return (
        <>
            <select {...props.register("timeView", { required: true, min: 1 })} name="timeView" className={props.className} value={props.timeView} onChange={e => props.setTimeView(e.target.value)}>
                {props.data.map((item, index) => {
                    if (item.value == props.selected) {
                        return <option key={index} value={item.value} selected>{item.label}</option>
                    }
                    return <option key={index} value={item.value}>{item.label}</option>
                })}
            </select>
            {props.errors.timeView?.type === "required" && <p>Phần thông tin bắt buộc.</p>}
        </>
    )
}

const Form = () => {

    const [link, setLink] = useState("");
    const [numView, setNumView] = useState(30);
    const [note, setNote] = useState("");
    const [numComment, setNumComment] = useState(0);
    const [numReaction, setNumReaction] = useState(0);
    const [timeView, setTimeView] = useState(30);
    const [comments, setComments] = useState("");

    const dispatch = useDispatch();
    const { register, handleSubmit, formState: { errors }, reset } = useForm();

    function handleCreate(data, e) {
        let idPostProcessed = processIdPost(data.link);
        if (idPostProcessed == "") {
            return false;
        }

        dispatch(createBuffLivestream({
            uid: idPostProcessed,
            num_view: data.numView,
            time_view: data.timeView,
            num_comment: data.numComment,
            num_reaction: data.numReaction,
            comments: data.comments ? data.comments : "",
            note: data.note
        }));
    }

    const classes = useStyles();
    return (
        <div className="buff-livestream-form">
            <Typography className="heading" variant="h3">Thông tin buff</Typography>
            <form onSubmit={handleSubmit(handleCreate)}>
                <GridContainer>
                    <Grid item sm={3}>
                        <div className="control-label">Link Livestream</div>
                    </Grid>
                    <Grid item sm={7}>
                        <input
                            {...register("link", { required: true })}
                            type="text"
                            value={link}
                            onChange={e => setLink(e.target.value)}
                            className="form-control"
                        />
                        {errors.link?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                    </Grid>
                </GridContainer>
                <GridContainer>
                    <Grid item sm={3}>
                        <div className="control-label">Số lượng mắt</div>
                    </Grid>
                    <Grid item sm={7}>
                        <input
                            {...register("numView", { required: true, min: 30 })}
                            type="number"
                            value={numView}
                            onChange={e => setNumView(e.target.value)}
                            className="form-control"
                        />
                        {errors.numView?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                        {errors.numView?.type === "min" && <p className="form-validate-error">Số lượng mắt tối thiểu 30.</p>}
                    </Grid>
                </GridContainer>
                <GridContainer>
                    <Grid item sm={3}>
                    </Grid>
                    <Grid item sm={7}>
                        <Alert variant="filled" severity="success" style={{ justifyContent: "center" }} className={classes.infoAlert}>
                            Tổng tiền buff mắt view = (Số lượng) x (Số phút) x (Giá buff)
                        </Alert>
                    </Grid>
                </GridContainer>
                <GridContainer>
                    <Grid item sm={3}>
                        <div className="control-label">Số phút xem</div>
                    </Grid>
                    <Grid item sm={7}>
                        <Select
                            timeView={timeView}
                            setTimeView={setTimeView}
                            errors={errors}
                            register={register}
                            data={TIME_LIVESTREAM}
                            className="form-control col-lg-7"
                        />
                    </Grid>
                </GridContainer>
                <GridContainer>
                    <Grid item sm={3}>
                        <div className="control-label">Số lượng comment</div>
                    </Grid>
                    <Grid item sm={7}>
                        <input
                            {...register("numComment", { required: true, min: 0 })}
                            type="number"
                            value={numComment}
                            onChange={e => setNumComment(e.target.value)}
                            className="form-control"
                        />
                        {errors.numComment?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                        {errors.numComment?.type === "min" && <p className="form-validate-error">Số lượng comment không được nhỏ hơn 0.</p>}
                    </Grid>
                </GridContainer>
                {
                    numComment > 0 &&
                    (
                        <GridContainer>
                            <Grid item sm={3}>
                                <div className="control-label">Nội dung</div>
                            </Grid>
                            <Grid item sm={7}>
                                <textarea
                                    {...register("comments", { required: true })}
                                    className="form-control"
                                    value={comments}
                                    onChange={e => setComments(e.target.value)}
                                    rows={10}
                                    placeholder="Nhập nội dung bạn muốn tăng bình luận, mỗi nội dung 1 dòng."
                                ></textarea>
                                {errors.comments?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                            </Grid>
                        </GridContainer>
                    )
                }
                <GridContainer>
                    <Grid item sm={3}>
                        <div className="control-label">Số lượng reaction</div>
                    </Grid>
                    <Grid item sm={7}>
                        <input
                            {...register("numReaction", { required: true })}
                            type="number"
                            value={numReaction}
                            onChange={e => setNumReaction(e.target.value)}
                            className="form-control"
                        />
                        {errors.numReaction?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                    </Grid>
                </GridContainer>
                <GridContainer>
                    <Grid item sm={3}>
                    </Grid>
                    <Grid item sm={7}>
                        <p className="emoji-icon">👍🥰😁😂</p>
                    </Grid>
                </GridContainer>
                <GridContainer>
                    <Grid item sm={3}>
                        <div className="control-label">Ghi chú</div>
                    </Grid>
                    <Grid item sm={7}>
                        <input
                            {...register("note")}
                            type="text"
                            value={note}
                            onChange={e => setNote(e.target.value)}
                            className="form-control"
                        />
                    </Grid>
                </GridContainer>
                <GridContainer>
                    <Grid item sm={3}>
                    </Grid>
                    <Grid item sm={7}>
                        <Alert variant="filled" severity="info" style={{ justifyContent: "center" }} className={classes.infoAlert}>
                            Tổng: {numView * timeView * 10 + numComment * 100 + numReaction * 40} nCoin<br />
                            Gía buff:<br />
                            10 nCoin/mắt/phút<br />
                            100 nCoin/comment<br />
                            40 nCoin/reaction
                        </Alert>
                    </Grid>
                </GridContainer>
                <div className="btn-purchase">
                    <button className="btn btn-success" type="submit">
                        Thanh toán
                    </button>
                </div>
            </form>
        </div>
    )
}

export default Form;