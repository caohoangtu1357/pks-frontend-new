import { Box, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React, { useState, useEffect } from 'react';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { getBuffLiveStreamAll } from '../../../../redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import BootstrapTable from "react-bootstrap-table-next";
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import { StatusColumn } from '@custom/components/Facebook/BuffStatus';
import GridContainer from '@jumbo/components/GridContainer';
import SearchBox from '@custom/components/System/SearchBox';

const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(2),
        minWidth: 120,
        verticalAlign: "middle"
    },
    selectEmpty: {
        marginTop: theme.spacing(4),
    },
}));

function List(props) {

    const [page, setPage] = useState(1);
    const [sizePerPage, setSizePerPage] = useState(10);
    const [sort, setSort] = useState('created_at');
    const [order, setOrder] = useState('desc');
    const { buffLivestream, allBuffLivestream } = useSelector(({ buffLivestream }) => buffLivestream);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getBuffLiveStreamAll({
            limit: sizePerPage,
            offset: 0,
            sort: sort,
            order: order,
        }));
    }, [buffLivestream])

    function handleSearch(str) {
        dispatch(getBuffLiveStreamAll({
            search: str,
            limit: sizePerPage,
            offset: 0,
            sort: sort,
            order: order,
        }));
    }

    function onTableChange(type, newState) {
        let sortField = newState.sortField?newState.sortField:sort;
        dispatch(getBuffLiveStreamAll({
            sort: sortField,
            order: newState.sortOrder,
            limit: newState.sizePerPage,
            offset: (newState.page - 1) * newState.sizePerPage,
        }));
        setSort(sortField);
        setOrder(newState.sortOrder);
        setPage(newState.page);
        setSizePerPage(newState.sizePerPage);
    }

    const paginateOptions = {
        page: page,
        sizePerPage: sizePerPage,
        showTotal: true,
        totalSize: allBuffLivestream.length != 0 ? allBuffLivestream.total : 0,
        hidePageListOnlyOnePage: true,
        paginationTotalRenderer: (from, to, size) => {
            return (
                <span className="react-bootstrap-table-pagination-total"> Hiển thị từ {from} đến {to} của {size}</span>
            )
        }
    };

    const columns = [{
        dataField: 'uid',
        text: 'Link livestream',
        sort: true
    }, {
        dataField: 'num_view',
        text: 'SL View',
        sort: true
    }, {
        dataField: 'num_comment',
        text: 'SL Comment',
        sort: true,
        headerAlign: 'center',
        align: "center"
    }, {
        dataField: 'num_reaction',
        text: 'SL Reaction',
        sort: true,
        headerAlign: 'center',
        align: "center"
    }, {
        dataField: 'status',
        text: 'Trạng thái',
        sort: true,
        headerAlign: 'center',
        align: "center",
        formatter: (cell, row, index) => (<StatusColumn status={row.status} />)
    }];

    return (
        <Box padding={3}>
            <GridContainer>
                <Grid sm={12}>
                    <SearchBox handleSearch={handleSearch} />
                </Grid>
                <Grid sm={12}>
                    <BootstrapTable
                        onTableChange={onTableChange}
                        keyField='_id'
                        remote={true}
                        data={allBuffLivestream.length != 0 ? allBuffLivestream.rows : []}
                        columns={columns}
                        pagination={paginationFactory(paginateOptions)}
                        keyBoardNav
                    />
                </Grid>
            </GridContainer>
        </Box>
    )
}

export default List;