import GridContainer from '@jumbo/components/GridContainer';
import { Box, Grid } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from 'react-bootstrap-table2-paginator';
import { getBuffLikeAll } from '../../../../redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import { StatusColumn } from '@custom/components/Facebook/BuffStatus';
import SearchBox from '@custom/components/System/SearchBox';

function List(props) {

    const [page, setPage] = useState(1);
    const [sizePerPage, setSizePerPage] = useState(10);
    const [sort, setSort] = useState('created_at');
    const [order, setOrder] = useState('desc');
    const [search, setSearch] = useState('');

    const { buffLike, allBuffLike } = useSelector(({ buffLike }) => buffLike);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getBuffLikeAll({
            limit: sizePerPage,
            offset: 0,
            sort: sort,
            order: order,
        }))
    }, [buffLike]);

    function handleSearch(str){
        dispatch(getBuffLikeAll({
            search:str,
            limit: sizePerPage,
            offset: 0,
            sort: sort,
            order: order,
        }));
    }

    function onTableChange(type, newState) {
        let sortField = newState.sortField?newState.sortField:sort;
        dispatch(getBuffLikeAll({
            sort: sortField,
            order: newState.sortOrder,
            limit: newState.sizePerPage,
            offset: (newState.page - 1) * newState.sizePerPage,
        }));
        setSort(sortField);
        setOrder(newState.sortOrder);
        setPage(newState.page);
        setSizePerPage(newState.sizePerPage);
    }

    const paginateOptions = {
        page: page,
        sizePerPage: sizePerPage,
        showTotal: true,
        totalSize: allBuffLike.length != 0 ? allBuffLike.total : 0,
        hidePageListOnlyOnePage: true,
        paginationTotalRenderer: (from, to, size) => {
            return (
                <span className="react-bootstrap-table-pagination-total"> Hiển thị từ {from} đến {to} của {size}</span>
            )
        }
    };

    const columns = [{
        dataField: 'uid',
        text: 'ID Bài viết',
        sort: true
    }, {
        dataField: 'number',
        text: 'Số lượng',
        sort: true
    }, {
        dataField: 'status',
        text: 'Trạng thái',
        sort: true,
        headerAlign: 'center',
        align: "center",
        formatter: (cell, row, index) => (<StatusColumn status={row.status} />)
    }];

    return (
        <Box padding={3}>
            <GridContainer>
                <Grid sm={12}>
                    <SearchBox handleSearch={handleSearch}/>
                </Grid>
                <Grid sm={12}>
                    <BootstrapTable
                        onTableChange={onTableChange}
                        keyField='_id'
                        remote={true}
                        data={allBuffLike.length != 0 ? allBuffLike.rows : []}
                        columns={columns}
                        pagination={paginationFactory(paginateOptions)}
                        keyBoardNav
                    />
                </Grid>
            </GridContainer>
        </Box>

    )
}

export default List;