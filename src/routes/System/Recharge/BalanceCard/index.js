import { AccountBalance } from '@material-ui/icons';
import React from 'react';
import CounterCard from '../../../../@jumbo/components/Common/CounterCard';

const BalaceCard = (props) => {
  return (
    <CounterCard
      icon={<AccountBalance style={{ fontSize: "3em" }} />}
      number={props.number}
      label={props.label}
      labelProps={{
        fontSize: 16,
      }}
      backgroundColor={props.backgroundColor}
      gradientDirection="180deg"
    />
  );
};

export default BalaceCard;