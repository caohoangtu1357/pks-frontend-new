import GridContainer from '@jumbo/components/GridContainer';
import PageContainer from '@jumbo/components/PageComponents/layouts/PageContainer';
import { Grid, makeStyles, Paper } from '@material-ui/core';
import React from 'react';
import Tab from './Tab';
import BalaceCard from './BalanceCard';
import { useSelector } from 'react-redux';

const useStyles = makeStyles({
    root: {
        flexGrow: 1,
        padding: "20px 10px"
    }
});

const breadcrumbs = [
    { label: 'Hệ thống', link: '#' },
    { label: 'Nạp tiền', link: '/system/recharge' },
];

const Recharge = () => {
    const classes = useStyles();

    const {recharge} = useSelector(({recharge})=>recharge);
    const {user} = useSelector(({user})=>user);

    return (
        <PageContainer heading="Nạp tiền" breadcrumbs={breadcrumbs}>
            <GridContainer>
                <Grid item sm={9}>
                    <Paper square className={classes.root} elevation={3}>
                        <Tab />
                    </Paper>
                </Grid>
                <Grid item sm={3}>
                    <Paper square className={classes.root} elevation={3}>
                        <GridContainer>
                            <Grid item xs={12}>
                                <BalaceCard number={user.balance} label="Số dư" backgroundColor={['#8E49F0 -18.96%', '#4904AB 108.17%']} />
                            </Grid>
                            <Grid item xs={12}>
                                <BalaceCard number={recharge.total} label="Tổng nạp" backgroundColor={['#5AB9FE -18.96%', '#1372B7 108.17%']} />
                            </Grid>
                            <Grid item xs={12}>
                                <BalaceCard number={recharge.total_amount_current_month} label="Tổng nạp trong tháng" backgroundColor={['#F25247 -18.96%', '#B72D23 108.17%']} />
                            </Grid>
                        </GridContainer>
                    </Paper>
                </Grid>
            </GridContainer>
        </PageContainer>
    )
}

export default Recharge;