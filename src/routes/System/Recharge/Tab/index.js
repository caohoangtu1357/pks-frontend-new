import React from 'react';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { Box, Typography } from '@material-ui/core';
import { AccountBalance, HistorySharp } from '@material-ui/icons';
import ChargeInfo from './ChargeInfo';
import ChargeTable from './ChargeTable';

const useStyles = makeStyles({
    root: {
        flexGrow: 1,
        padding: "20px 10px"
    },
    infoAlert: {
        "& .MuiAlert-icon": {
            display: "none"
        }
    },
    bankImage: {
        maxHeight: 70
    }
});

function a11yProps(index) {
    return {
        id: `nav-tab-${index}`,
        'aria-controls': `nav-tabpanel-${index}`,
    };
}

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <Box
            role="tabpanel"
            hidden={value !== index}
            id={`nav-tabpanel-${index}`}
            aria-labelledby={`nav-tab-${index}`}
            {...other}>
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </Box>
    );
}

function LinkTab(props) {
    return (
        <Tab
            component="a"
            onClick={event => {
                event.preventDefault();
            }}
            {...props}
        />
    );
}

const IconLabelTabs = () => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <>
            <Paper square className={classes.root}>
                <Box style={{ display: "flex", justifyContent: "flex-end" }}>
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        variant="fullWidth"
                        indicatorColor="secondary"
                        textColor="secondary"
                        aria-label="icon label tabs example">
                        <LinkTab style={{ maxWidth: 200 }} icon={<AccountBalance />} label="Ngân hàng" href="/drafts" {...a11yProps(0)} />
                        <LinkTab style={{ maxWidth: 200 }} icon={<HistorySharp />} label="Lịch sử nạp" href="/trash" {...a11yProps(1)} />
                    </Tabs>
                </Box>
                <TabPanel value={value} index={0}>
                    <ChargeInfo />
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <ChargeTable />
                </TabPanel>
            </Paper>

        </>
    );
}

export default IconLabelTabs;