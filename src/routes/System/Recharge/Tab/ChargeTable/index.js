import React, { useState, useEffect } from 'react';
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from 'react-bootstrap-table2-paginator';
import { getRechargeAll } from '../../../../../redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import { StatusColumn } from '@custom/components/System/RechargeStatus'

function ChargeTable(props) {

    const [page, setPage] = useState(1);
    const [sizePerPage, setSizePerPage] = useState(10);
    const [sort, setSort] = useState('created_at');
    const [order, setOrder] = useState('desc');
    const dispatch = useDispatch();
    const { allRecharge } = useSelector(({ recharge }) => recharge);

    useEffect(() => {
        dispatch(getRechargeAll({
            limit: sizePerPage,
            offset: 0,
            sort: sort,
            order: order,
        }));
    }, [])

    function onTableChange(type, newState) {
        let sortField = newState.sortField?newState.sortField:sort;
        dispatch(getRechargeAll({
            sort: sortField,
            order: newState.sortOrder,
            limit: newState.sizePerPage,
            offset: (newState.page - 1) * newState.sizePerPage,
        }));
        setSort(sortField);
        setOrder(newState.sortOrder);
        setPage(newState.page);
        setSizePerPage(newState.sizePerPage);
    }

    const paginateOptions = {
        page: page,
        sizePerPage: sizePerPage,
        showTotal: true,
        totalSize: allRecharge.length != 0 ? allRecharge.total : 0,
        hidePageListOnlyOnePage: true,
        paginationTotalRenderer: (from, to, size) => {
            return (
                <span className="react-bootstrap-table-pagination-total"> Hiển thị từ {from} đến {to} của {size}</span>
            )
        }
    };

    const columns = [{
        dataField: 'content',
        text: 'Nội dung',
        sort: true
    }, {
        dataField: 'phone',
        text: 'SĐT',
        sort: true
    }, {
        dataField: 'amount',
        text: 'Tổng số',
        sort: true
    }, {
        dataField: 'is_activated',
        text: 'Trạng thái',
        sort: true,
        headerAlign: 'center',
        align: "center",
        formatter: (cell, row, index) => (<StatusColumn status={row.is_activated} />)
    }, {
        dataField: 'created_at',
        text: 'Ngày nạp',
        sort: true
    },];

    if (allRecharge.length == 0) {
        return (
            <div style={{ textAlign: "center" }}>
                <p>Bạn chưa có giao dịch nào được thực hiện</p>
            </div>
        )
    } else {
        if (allRecharge.total == 0) {
            return (
                <div style={{ textAlign: "center", fontSize: 16 }}>
                    <p>Bạn chưa có giao dịch nào được thực hiện</p>
                </div>
            )
        }
        return (
            <BootstrapTable
                onTableChange={onTableChange}
                keyField='_id'
                remote={true}
                data={allRecharge.length != 0 ? allRecharge.rows : []}
                columns={columns}
                pagination={paginationFactory(paginateOptions)}
                keyBoardNav
            />
        )
    }
}

export default ChargeTable;