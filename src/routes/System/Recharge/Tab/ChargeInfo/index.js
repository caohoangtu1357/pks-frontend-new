import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
import GridContainer from '@jumbo/components/GridContainer';
import { Alert } from '@material-ui/lab';

const useStyles = makeStyles({
    infoAlert:{
        "& .MuiAlert-icon": {
            display:"none"
        }
    },
    bankImage:{
        maxHeight:70
    }
});

const ChargeInfo = () =>{
    
    const classes = useStyles();

    return (
        <GridContainer>
            <Grid item xs={12}>
                <Typography variant="h1" style={{color:"#128ef2"}}>
                    Tỷ giá: 1 VND = 1 nCoin
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <Alert variant="filled" severity="info" className={classes.infoAlert}>
                    Bạn vui lòng chuyển khoản chính xác nội dung chuyển khoản bên dưới hệ thống sẽ tự động cộng tiền cho bạn sau 1 - 5 phút sau khi nhận được tiền. Nếu chuyển khác ngân hàng sẽ mất thời gian lâu hơn, tùy thời gian xử lý của mỗi ngân hàng. Nếu sau 10 phút từ khi tiền trong tài khoản của bạn bị trừ mà vẫn chưa được cộng tiền vui liên hệ Admin để được hỗ trợ.
                </Alert>
            </Grid>
            <Grid item xs={12}>
                <GridContainer style={{textAlign:"center", alignItems:"center"}}>
                    <Grid item xs={6}>
                        <img src="/images/bank/tpbank.png" className={classes.bankImage}/>
                    </Grid>
                    <Grid item xs={6}>
                        <img src="/images/bank/momo.png" className={classes.bankImage}/>
                    </Grid>
                </GridContainer>
            </Grid>
            <Grid item xs={12}>
                <GridContainer>
                    <Grid item xs={6}>
                        <Alert serverty="success">
                            Số tài khoản: 0123456789<br/>
                            Tên tài khoản: PHAM MINH TIEN<br/>
                            Ngân hàng: Tiên Phong Bank
                        </Alert>
                    </Grid>
                    <Grid item xs={6}>
                        <Alert serverty="success">
                            Số điện thoại: 0867870179<br/>
                            Tên tài khoản: PHAM MINH TIEN<br/>
                            Số tiền tối thiểu: 20,000vnđ
                        </Alert>
                    </Grid>
                </GridContainer>
            </Grid>
            <Grid item xs={12}>
                <Typography variant="h1" style={{color:"#128ef2"}}>
                    Nội dung chuyển khoản
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <Alert variant="filled" severity="info" style={{justifyContent:"center"}} className={classes.infoAlert}>
                    NAP 0961675735
                </Alert>
            </Grid>
            <Grid item xs={12}>
                <Alert variant="filled" severity="warning" className={classes.infoAlert}>
                    Lưu ý: Nạp sai cú pháp hoặc sai số tài khoản sẽ bị trừ 10% phí giao dịch, tối đa trừ 50.000 nCoin. Ví dụ nạp sai 100.000 trừ 10.000, 200.000 trừ 20.000 , 500.000 trừ 50.000, 1 triệu trừ 50.000, 10 triệu trừ 50.000...
                </Alert>
            </Grid>
        </GridContainer>
    )
}

export default ChargeInfo;