import React, { useContext } from 'react';
import CmtMediaObject from '../../../../@coremat/CmtMediaObject';
import GridEmptyResult from '../../../../@coremat/CmtGridView/GridEmptyResult';
import CmtGridPagination from '../../../../@coremat/CmtGridView/CmtGridPagination';
import Box from '@material-ui/core/Box';
import { Grid, Paper, Typography } from '@material-ui/core';
import { alpha } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/styles';
import { JumboCard } from '../../../../@jumbo/components/Common';
import GridContainer from '@jumbo/components/GridContainer';
import { AcUnitRounded, LocalAtmRounded, RssFeed, ThumbUpRounded } from '@material-ui/icons';
import './index.scss';

const useStyles = makeStyles(theme => ({
  itemRoot: {
    padding: 1,
    '& .Cmt-avatar': {
      height: 34,
      width: 34,
      cursor: 'pointer',
    },
    '& .Cmt-media-header': {
      marginBottom: 8,
    },
    '& .Cmt-media-image': {
      marginRight: 4,
      marginTop: 0,
    },
    '& .titleRoot': {
      fontSize: 16,
    },
  },
  badgePrice: {
    cursor: 'pointer',
    fontSize: 14,
    backgroundColor: alpha(theme.palette.primary.main, 0.1),
    color: theme.palette.primary.main,
    padding: '4px 10px',
    borderRadius: 4,
    display: 'block',
  },
  typoList: {
    marginRight: 15,
  },
}));

const StaticItem = (props) => {
  const classes = useStyles();
  return (
    <Box className={classes.itemRoot} className="item-statistic">
      <Paper className="paper-icon">
        <GridContainer>
          <Grid item sm={6}>
            <Box className="box-icon" style={{ backgroundColor: props.background }}>
              {props.icon}
            </Box>
          </Grid>
          <Grid item sm={6}>
            <Box className="box-info">
              <Typography style={{fontWeight:600,fontSize:14}}>{props.number}</Typography>
              <Typography style={{fontSize:14}}>{props.title}</Typography>
            </Box>
          </Grid>
        </GridContainer>
      </Paper>
    </Box >
  )
}

const Statistics = () => {
  const classes = useStyles();
  return (
    <Paper className="right-statisic-wrapper">
      <GridContainer>
        <Grid item xs={12}>
          <GridContainer>
            <Grid item xs={6}>
              <StaticItem number={6} title={"Follow"} background={"#e6effc"} icon={<RssFeed className={"icon"} style={{ color: "#2f7be5" }} />} />
            </Grid>
            <Grid item xs={6}>
              <StaticItem number={6} title={"Like page"} background={"#e5f7fe"} icon={<ThumbUpRounded className={"icon"} style={{ color: "#5cbdfc" }} />} />
            </Grid>
          </GridContainer>
        </Grid>
        <Grid item xs={12}>
          <GridContainer>
            <Grid item xs={6}>
              <StaticItem number={6} title={"Nạp tháng"} background={"#e0faef"} icon={<LocalAtmRounded className={"icon"} style={{ color: "#59e22d" }} />} />
            </Grid>
            <Grid item xs={6}>
              <StaticItem number={"Giảm 0%"} title={"Thành viên"} background={"#fcf0e8"} icon={<AcUnitRounded className={"icon"} style={{ color: "#2f7be5" }} />} />
            </Grid>
          </GridContainer>
        </Grid>

      </GridContainer>
    </Paper>
  )
}

export default Statistics;
