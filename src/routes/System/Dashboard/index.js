import CmtAvatar from '@coremat/CmtAvatar';
import CmtCard from '@coremat/CmtCard';
import CmtCardHeader from '@coremat/CmtCard/CmtCardHeader';
import GridContainer from '@jumbo/components/GridContainer';
import PageContainer from '@jumbo/components/PageComponents/layouts/PageContainer';
import { Grid } from '@material-ui/core';
import { MoreVertRounded } from '@material-ui/icons';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CardStatistic from './CardStatistic';
import Newsfeed from './NewsFeed';
import RightStatistic from './Statistic';
import { getRecharge } from '../../../redux/actions';
import Notify from './Notify';

const breadcrumbs = [
    { label: 'Hệ thống', link: '#' },
    { label: 'Trang chủ', link: '/system/' },
];

const Dashboard = () => {

    const { user } = useSelector(({ user }) => user);
    const { recharge } = useSelector(({ recharge }) => recharge);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getRecharge());
    }, []);

    return (
        <PageContainer heading="Trang chủ" breadcrumbs={breadcrumbs}>
            <GridContainer>
                <Grid item sm={8}>
                    <Grid item sm={12} style={{ marginBottom: 20 }}>
                        <CmtCard>
                            <CmtCardHeader style={{ padding: 10 }}
                                avatar={<CmtAvatar src={'/images/auth/avatar-icon.jpg'} size="large" alt="Avatar" />}
                                title={user.full_name}
                                subTitle={'Thành viên'}
                                actionHandleIcon={<MoreVertRounded fontSize="default" />}>
                            </CmtCardHeader>
                        </CmtCard>
                    </Grid>
                    <Grid item sm={12} style={{ marginBottom: 20 }}>
                        <GridContainer>
                            <Grid item sm={6} style={{width:"100%"}}>
                                <CardStatistic title="Số dư hiện tại" backgroundColor="#7D38DF" amount={user.balance} />
                            </Grid>
                            <Grid item sm={6} style={{width:"100%"}}>
                                <CardStatistic title="Tổng tiền nạp" backgroundColor="#7D38DF" amount={recharge.total} />
                            </Grid>
                        </GridContainer>
                    </Grid>
                    <Grid item sm={12}>
                        <Newsfeed />
                    </Grid>
                </Grid>
                <Grid item sm={4}>
                    <GridContainer>
                        <Grid item sm={12}>
                            <RightStatistic />
                        </Grid>
                        <Grid item sm={12} style={{width:"100%"}}>
                            <Notify />
                        </Grid>
                    </GridContainer>
                </Grid>
            </GridContainer>
        </PageContainer>
    )
}

export default Dashboard;