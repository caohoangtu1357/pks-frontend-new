import { StatisticsCard } from '@jumbo/components/Common';
import { makeStyles } from '@material-ui/core';
import React from 'react';
import PropertiesGraph from './PropertiesStatistics/PropertiesGraph';
import {numberWithCommas} from '@custom/config/General';

const useStylesCard = makeStyles(theme => ({
    cardRoot: {
      color: theme.palette.common.white,
    },
    titleRoot: {
      fontSize: 12,
      fontWeight: 400,
      textTransform: 'uppercase',
      letterSpacing: 0.4,
    },
  }));

const CardStatistic = (props) =>{
    const classes = useStylesCard();
    return(
        <StatisticsCard
            className={classes.cardRoot}
            backgroundColor={props.backgroundColor}
            title={props.title}
            titleProps={{
                variant: 'inherit',
                component: 'h4',
                className: classes.titleRoot,
            }}
            amount={props.amount?numberWithCommas(props.amount)+" nCoin":"0 nCoin"}
            description={props.description}
        >
            <PropertiesGraph />
        </StatisticsCard>
    )
}

export default CardStatistic;