import React, { useEffect } from 'react';
import CmtList from '../../../../../@coremat/CmtList';
import PostItem from './PostItem';
import { useDispatch, useSelector } from 'react-redux';
import Box from '@material-ui/core/Box';
import {loadMorePost} from '../../../../../redux/actions';

const PostsList = () => {

  const {posts} = useSelector(({post})=>post);
  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(loadMorePost({offset: 0}));
  },[]);

  return (
    <CmtList
      data={posts}
      renderRow={(item, index) => (
        <Box mb={6} key={index}>
          <PostItem item={item} />
        </Box>
      )}
    />
  );
};

export default PostsList;
