import React from 'react';
import Box from '@material-ui/core/Box';
import CmtAvatar from '../../../../../../@coremat/CmtAvatar';
import Typography from '@material-ui/core/Typography';
import { getTimeDiffString } from '../../../../../../@jumbo/utils/dateHelper';
import PropTypes from 'prop-types';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Popper from '@material-ui/core/Popper';
import CmtCard from '../../../../../../@coremat/CmtCard';
import { getContactImageUrl, getPostImage } from '@custom/config/General';
import { CheckCircle } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  titleRoot: {
    letterSpacing: 1.25,
    color: "#2c7be5"
  },
  subTitleRoot: {
    fontSize: 12,
    color: theme.palette.text.disabled,
  },
}));

const UserInfo = (props) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const item = props.item;

  const openPopper = event => {
    setAnchorEl(event.currentTarget);
  };

  const closePopper = event => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? `user-popper-${item._id}` : undefined;

  return (
    <Box display="flex" alignItems="center">
      <Box mr={4} onMouseEnter={openPopper} onMouseLeave={closePopper} aria-describedby={id}>
        <CmtAvatar size={40} src={"/images/auth/admin.png"} alt={"Quản trị viên"} />
      </Box>
      <Box>
        <Typography className={classes.titleRoot} component="div" variant="h5">
          {"Quản trị viên"} <CheckCircle style={{ fontSize: "1.1em" }} />
        </Typography>
        <Typography className={classes.subTitleRoot}>{(item.created_at)}</Typography>
      </Box>
    </Box>
  );
};

export default UserInfo;
