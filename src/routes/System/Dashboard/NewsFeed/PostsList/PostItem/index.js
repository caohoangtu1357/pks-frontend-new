import React from 'react';
import CmtCard from '../../../../../../@coremat/CmtCard';
import CmtCardHeader from '../../../../../../@coremat/CmtCard/CmtCardHeader';
import Box from '@material-ui/core/Box';
import UserInfo from './UserInfo';
import CmtCardContent from '../../../../../../@coremat/CmtCard/CmtCardContent';
import PostStats from './PostStats';
import CmtList from '../../../../../../@coremat/CmtList';
import CommentItem from './CommentItem';
import AddComment from './AddComment';
import Attachments from './Attachments';
import PropTypes from 'prop-types';
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles(() => ({}));

const PostItem = ({ item }) => {
  const classes = useStyles();
  return (
    <CmtCard>
      <CmtCardHeader title={<UserInfo item={item} />} />
      <CmtCardContent onClick={() => { return false; }}>
        <Box>
          <Box mb={2} component="p">
            <div style={{ wordWrap: "break-word", whiteSpace: "break-spaces" }}>
              {<div dangerouslySetInnerHTML={{ __html: (item.content) }} />}
            </div>
          </Box>
          <Attachments items={[item]} />
          <PostStats item={item} />
          {item.comments.length > 0 && (
            <CmtList
              data={item.comments}
              renderRow={(item, index) => <CommentItem key={index} item={item} classes={classes} />}
            />
          )}
          <AddComment postId={item._id} />
        </Box>
      </CmtCardContent>
    </CmtCard>
  );
};

export default PostItem;

PostItem.prototype = {
  item: PropTypes.object.isRequired,
};
