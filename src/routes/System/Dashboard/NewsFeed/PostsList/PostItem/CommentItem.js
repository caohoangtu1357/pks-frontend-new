import React, { useState } from 'react';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import CommentUserInfo from './CommentUserInfo';

const CommentItem = ({ item, classes ,user}) => {
  return (
    <Box mb={6}>
      <CommentUserInfo user={item} classes={classes} />
      <Box ml={14}>
        <Box mt={2} component="p" color="text.secondary">
          {item.comment}
        </Box>
      </Box>
    </Box>
  );
};

export default CommentItem;

CommentItem.prototype = {
  item: PropTypes.object.isRequired,
};
