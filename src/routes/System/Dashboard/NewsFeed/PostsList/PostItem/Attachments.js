import React, { useState } from 'react';
import Box from '@material-ui/core/Box';
import CmtGridView from '../../../../../../@coremat/CmtGridView';
import CmtImage from '../../../../../../@coremat/CmtImage';
import { alpha, makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { getPostImage } from '@custom/config/General';
import MediaViewer from '../../../../../Apps/Chat/MediaViewer';

const useStyles = makeStyles(theme => ({
  imgView: {
    cursor: 'pointer',
    borderRadius: 4,
    overflow: 'hidden',
    position: 'relative',
    maxHeight: 540,
    height: '100%',
    '& img': {
      width: '100%',
      height: '100%',
      objectFit: 'cover',
      display: 'block',
    },
  },
  imgCount: {
    position: 'absolute',
    left: 0,
    top: 0,
    zIndex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: alpha(theme.palette.common.black, 0.5),
    color: theme.palette.common.white,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 18,
  },
}));

const Attachments = ({ items }) => {
  const classes = useStyles();
  const [position, setPosition] = useState(-1);

  const handleClose = () => {
    setPosition(-1);
  };

  return (
    <Box mb={5}>
      <CmtGridView
        onClick={() => { return false; }}
        itemPadding={24}
        data={items.length > 4 ? items.slice(0, 4) : items}
        column={items.length > 3 ? 2 : items.length}
        renderRow={(item, index) => (
          <Box key={index} className={classes.imgView} onClick={() => setPosition(index)}>
            <CmtImage src={getPostImage(item.image_path)} alt="attachment" />
            {items.length > 4 && index === 3 && <Box className={classes.imgCount}>+ {items.length - 3}</Box>}
          </Box>
        )}
      />
      <MediaViewer position={position} medias={items} handleClose={handleClose} />
    </Box>
  );
};

export default Attachments;
