import React from 'react';
import Box from '@material-ui/core/Box';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import makeStyles from '@material-ui/styles/makeStyles/makeStyles';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import CommentIcon from '@material-ui/icons/Comment';
import ShareIcon from '@material-ui/icons/Share';
import { convertNumberToK } from '@custom/config/General';

const useStyles = makeStyles(() => ({
  iconSm: {
    fontSize: '16px !important',
    marginRight: 6,
    color: "#2c7be5"
  },
}));

const PostStats = ({ item }) => {
  const dispatch = useDispatch();
  const classes = useStyles();

  const { num_like, num_comment, num_share } = item;

  let likeText = "";
  if(item.comments.length > 0){
    if(num_like>0){
      likeText = item.comments[item.comments.length-1].name +" và " +convertNumberToK(num_like) +" người khác";
    }
  }else{
    likeText = num_like;
  }

  return (
    <Box display="flex" alignItems="center" flexWrap="wrap" mb={{ xs: 4, sm: 6 }} color="text.disabled" fontSize={15}>
      <Box display="flex" alignItems="center" mr={5} mb={{ xs: 2, sm: 0 }} className="pointer">
        <ThumbUpAltIcon className={classes.iconSm} />
        <Box ml={1}>{num_like > 0 && likeText}</Box>
      </Box>
      <Box display="flex" alignItems="center" mb={{ xs: 2, sm: 0 }} className="pointer">
        <CommentIcon className={classes.iconSm} />
        <Box ml={1}>{num_comment > 0 && convertNumberToK(num_comment)} comments</Box>
      </Box>
      <Box display="flex" alignItems="center" mb={{ xs: 2, sm: 0 }} ml="auto" className="pointer">
        <ShareIcon className={classes.iconSm} />
        <Box ml={1}>{num_share > 0 && convertNumberToK(num_share)} shares</Box>
      </Box>
    </Box>
  );
};

export default PostStats;

PostStats.prototype = {
  item: PropTypes.object.isRequired,
};
