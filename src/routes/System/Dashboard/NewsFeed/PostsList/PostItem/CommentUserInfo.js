import React from 'react';
import Box from '@material-ui/core/Box';
import CmtAvatar from '../../../../../../@coremat/CmtAvatar';
import Typography from '@material-ui/core/Typography';
import { getTimeDiffString } from '../../../../../../@jumbo/utils/dateHelper';
import PropTypes from 'prop-types';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Popper from '@material-ui/core/Popper';
import CmtCard from '../../../../../../@coremat/CmtCard';
import { getContactImageUrl, getPostImage } from '@custom/config/General';
import { CheckCircle } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  titleRoot: {
    letterSpacing: 1.25,
    fontWeight: "normal",
    color: "#2c7be5",
    fontSize:13
  },
  subTitleRoot: {
    fontSize: 12
  },
  content:{
    fontSize:13
  }
}));

const CommentUserInfo = ({ user }) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const openPopper = event => {
    setAnchorEl(event.currentTarget);
  };

  const closePopper = event => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? `user-popper-${user._id}` : undefined;

  return (
    <Box display="flex" alignItems="center">
      <Box mr={4} onMouseEnter={openPopper} onMouseLeave={closePopper} aria-describedby={id}>
        <a href={user.facebook_link}>
          <CmtAvatar size={40} src={getPostImage(user.image_path)} />
        </a>
      </Box>
      <Box>
        <Typography className={classes.titleRoot} component="div" variant="h4">
          <a href={user.facebook_link}>{user.name} <CheckCircle style={{ fontSize: "0.8em" }} /></a>
        </Typography>
        <Typography variant="body2" className={classes.content} gutterBottom>
          {user.content}
        </Typography>
        <Typography className={classes.subTitleRoot}>{"2 giờ trước"}</Typography>
      </Box>
    </Box>
  );
};

export default CommentUserInfo;
