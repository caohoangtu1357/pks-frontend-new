import React, { useContext, useEffect } from 'react';
import PostsList from './PostsList';
import { useDispatch } from 'react-redux';
import { getWallHeight } from '../../../../@jumbo/constants/AppConstants';
import makeStyles from '@material-ui/core/styles/makeStyles';
import AppContext from '../../../../@jumbo/components/contextProvider/AppContextProvider/AppContext';
import { GET_FEED_POSTS } from '@jumbo/constants/ActionTypes';
import { postsList, user } from '@fake-db/apps/wall';


const useStyles = makeStyles(() => ({
  perfectScrollbarNewsFeed: {
    height: props => `calc(100vh - ${props.height}px)`,
  },
}));

const NewsFeed = width => {
  const { showFooter } = useContext(AppContext);
  const dispatch = useDispatch();
  const classes = useStyles({
    height: getWallHeight(width, showFooter),
  });

  useEffect(() => {
    dispatch({ type: GET_FEED_POSTS, payload: postsList });
  }, [dispatch]);

  return (
    <PostsList />
  );
};

export default NewsFeed;
