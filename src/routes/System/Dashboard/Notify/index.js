import React, { useEffect } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';

import makeStyles from '@material-ui/core/styles/makeStyles';

import CmtList from '../../../../@coremat/CmtList';

import { crm } from '../../../../@fake-db';
import TicketsItem from './TicketsItem';
import JumboCard from '../../../../@jumbo/components/Common/JumboCard';
import { useDispatch, useSelector } from 'react-redux';
import { getNotify } from '../../../../redux/actions';

const useStyles = makeStyles(() => ({
  perScrollbarRoot: {
    minHeight: 200,
    marginRight: -24,
    marginLeft: -24,
  },
}));

const Notifies = () => {

  const dispatch = useDispatch();
  const {notifies} = useSelector(({notify})=>notify);

  useEffect(() => {
    dispatch(getNotify());
  }, []);

  const classes = useStyles();
  return (
    <JumboCard title="Thông Báo Mới">
      <PerfectScrollbar className={classes.perScrollbarRoot}>
        <CmtList data={notifies} renderRow={(item, index) => <TicketsItem key={index} item={item} />} />
      </PerfectScrollbar>
    </JumboCard>
  );
};

export default Notifies;
