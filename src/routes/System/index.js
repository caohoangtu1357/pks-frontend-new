import React, { lazy, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router';
import PageLoader from '../../@jumbo/components/PageComponents/PageLoader';

const System = ({ match }) => {
  const requestedUrl = match.url.replace(/\/$/, '');
  return (
    <Suspense fallback={<PageLoader />}>
      <Switch>
        <Route exact path={`${requestedUrl}/`} component={lazy(() => import('./Dashboard'))} />
        
        <Route exact path={`${requestedUrl}/contact`} component={lazy(() => import('./Contact'))} />
        <Route exact path={`${requestedUrl}/contact/create`} component={lazy(() => import('./Contact/Create'))} />
        <Route exact path={`${requestedUrl}/contact/edit/:id`} component={lazy(() => import('./Contact/Edit'))} />
        
        <Route path={`${requestedUrl}/order`} component={lazy(() => import('./Order'))} />
        <Route path={`${requestedUrl}/recharge`} component={lazy(() => import('./Recharge'))} />
        <Route path={`${requestedUrl}/setting`} component={lazy(() => import('./Setting'))} />
        <Route component={lazy(() => import('../ExtraPages/404'))} />
      </Switch>
    </Suspense>
  );
};

export default System;
