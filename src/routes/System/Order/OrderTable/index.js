import React, { useState, useEffect } from 'react';
import { getAllOrder } from '../../../../redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import SearchBox from '@custom/components/System/SearchBox';
import { Box, Grid } from '@material-ui/core';
import GridContainer from '@jumbo/components/GridContainer';

function OrderTable(props) {

    const dispatch = useDispatch();
    const [page, setPage] = useState(1);
    const [sizePerPage, setSizePerPage] = useState(10);
    const [sort, setSort] = useState('created_at');
    const [sortOrder, setSortOrder] = useState('desc');
    const { order, allOrder } = useSelector(({ order }) => order);

    useEffect(() => {
        dispatch(getAllOrder({
            limit: sizePerPage,
            offset: 0,
            sort: sort,
            order: sortOrder,
        }));
    }, []);

    function onTableChange(type, newState) {
        let sortField = newState.sortField?newState.sortField:sort;
        dispatch(getAllOrder({
            sort: sortField,
            order: newState.sortOrder,
            limit: newState.sizePerPage,
            offset: (newState.page - 1) * newState.sizePerPage,
        }));
        setSort(sortField);
        setSortOrder(newState.sortOrder);
        setPage(newState.page);
        setSizePerPage(newState.sizePerPage);
    }

    function handleSearch(str) {
        dispatch(getAllOrder({
            search: str,
            limit: sizePerPage,
            offset: 0,
            sort: sort,
            order: order,
        }));
    }

    const paginateOptions = {
        page: page,
        sizePerPage: sizePerPage,
        showTotal: true,
        totalSize: allOrder.length != 0 ? allOrder.total : 0,
        hidePageListOnlyOnePage: true,
        paginationTotalRenderer: (from, to, size) => {
            return (
                <span className="react-bootstrap-table-pagination-total"> Hiển thị từ {from} đến {to} của {size}</span>
            )
        }
    };

    const columns = [{
        dataField: 'object_id',
        text: 'Mã giao dịch',
        sort: true,
        formatter: (cell, row, index) => ("#" + row.object_id)
    }, {
        dataField: 'object_type',
        text: 'Dịch vụ',
        sort: true
    }, {
        dataField: 'number',
        text: 'Số lượng',
        sort: true,
        headerAlign: 'center',
        align: "center"
    }, {
        dataField: 'price',
        text: 'Tổng tiền',
        sort: true,
        headerAlign: 'center',
        align: "center",
        formatter: (cell, row, index) => cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
    ];

    if (allOrder.length == 0) {
        return (
            <div style={{ textAlign: "center" }}>
                <p>Bạn chưa có giao dịch nào được thực hiện</p>
            </div>
        )
    } else {
        if (allOrder.total == 0) {
            return (
                <div style={{ textAlign: "center", fontSize: 16 }}>
                    <p>Bạn chưa có giao dịch nào được thực hiện</p>
                </div>
            )
        }
        return (
            <Box padding={3}>
                <GridContainer>
                    <Grid sm={12}>
                        <SearchBox handleSearch={handleSearch} />
                    </Grid>
                    <Grid sm={12}>
                        <BootstrapTable
                            onTableChange={onTableChange}
                            keyField='_id'
                            remote={true}
                            data={allOrder.length != 0 ? allOrder.rows : []}
                            columns={columns}
                            pagination={paginationFactory(paginateOptions)}
                            keyBoardNav
                        />
                    </Grid>
                </GridContainer>
            </Box>
        )
    }
}

export default OrderTable;