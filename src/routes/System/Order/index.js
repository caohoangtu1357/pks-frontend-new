import GridContainer from '@jumbo/components/GridContainer';
import PageContainer from '@jumbo/components/PageComponents/layouts/PageContainer';
import { Grid, Paper } from '@material-ui/core';
import React from 'react';
import OrderTable from './OrderTable';

const breadcrumbs = [
    { label: 'Hệ thống', link: '#' },
    { label: 'Lịch sử giao dịch', link: '/system/order' },
];

const Recharge = () => {
    return (
        <PageContainer heading="Lịch sử giao dịch" breadcrumbs={breadcrumbs}>
            <Paper style={{padding:20}}>
                <OrderTable />
            </Paper>
        </PageContainer>
    )
}

export default Recharge;