import GridContainer from '@jumbo/components/GridContainer';
import PageContainer from '@jumbo/components/PageComponents/layouts/PageContainer';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import AccountInfo from './AccountInfo';
import ChangePassword from './ChangePassword';
import Header from './Header';

const styles = makeStyles(theme => ({
  
}));

const breadcrumbs = [
    { label: 'Hệ thống', link: '#' },
    { label: 'Cài đặt tài khoản', link: '/system/setting' },
];


const Setting = () => {

    const classes = styles();

    return (
        <PageContainer heading="Cài đặt tài khoản" breadcrumbs={breadcrumbs}>
            <GridContainer>
                <Grid item sm={12} style={{overflow:"hidden"}}>
                    <Header/>
                </Grid>
            </GridContainer>
            <GridContainer>
                <Grid item sm={8}>
                    <AccountInfo/>
                </Grid>
                <Grid item sm={4}>
                    <ChangePassword/>
                </Grid>
            </GridContainer>
        </PageContainer>

    )
}

export default Setting;