import React, { useState } from 'react';
import { Box, Paper, Typography } from '@material-ui/core';
import "./index.scss";
import Button from '@material-ui/core/Button';
import SendIcon from '@material-ui/icons/Send';
import { useDispatch } from 'react-redux';
import {changePassword} from '../../../../redux/actions';

const ChangePassword = () => {

    const [newPassword, setNewPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [oldPassword, setOldPassword] = useState('');

    const dispatch = useDispatch();

    function handleChangePass(){
        dispatch(changePassword({
            old_password: oldPassword,
            new_password: newPassword,
            password_confirm: confirmPassword
        }));
    }

    return (
        <div className="change-password">
            <Paper className="paper" elevation={3}>
                <Typography className="heading" variant="h3">Thay đổi mật khẩu</Typography>
                <Box
                    component="form"
                >
                    <div className="form-group">
                        <label htmlFor="new_password" className="control-label">
                            Mật khẩu cũ
                        </label>
                        <input type="password" id="outlined-basic" value={oldPassword} onChange={e=>setOldPassword(e.target.value)} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="new_password" className="control-label">
                            Mật khẩu mới
                        </label>
                        <input type="password" id="outlined-basic" value={newPassword} onChange={e=>setNewPassword(e.target.value)} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="new_password" className="control-label">
                            Xác nhận mật khẩu mới
                        </label>
                        <input type="password" id="outlined-basic" className="form-control" onChange={e=>setConfirmPassword(e.target.value)} value={confirmPassword}/>
                    </div>
                    <div className="form-group">
                        <Button variant="contained" type="button" color="primary" onClick={handleChangePass} endIcon={<SendIcon />}>
                            Đổi mật khẩu
                        </Button>
                    </div>
                </Box>
            </Paper>
        </div>
    );
}

export default ChangePassword;