import React, { useState } from 'react';
import { Box, Grid, Paper, Typography } from '@material-ui/core';
import "./index.scss";
import { useSelector, useDispatch } from "react-redux";
import GridContainer from '@jumbo/components/GridContainer';
import {updateFbId} from '../../../../redux/actions';

const AccountInfo = () => {

    const { user } = useSelector(({ user }) => user);
    const dispatch = useDispatch();
    const [fbId,setFbId] = useState(user.fb_id);

    function handleUpdateFbId(){
        dispatch(updateFbId({fb_id:fbId}));    
    }

    return (
        <div className="account-info">
            <Paper className="paper" elevation={3}>
                <Typography className="heading" variant="h3">Thông tin tài khoản</Typography>
                <Box
                    component="form"
                >
                    <GridContainer>
                        <Grid item sm={6}>
                            <div className="form-group">
                                <label className="control-label">Họ và tên</label>
                                <input type="text" className="form-control" defaultValue={user.full_name} readOnly />
                            </div>
                            <div className="form-group">
                                <label className="control-label">Số điện thoại</label>
                                <input type="text" className="form-control" defaultValue={user.phone} readOnly />
                            </div>
                        </Grid>
                        <Grid item sm={6}>
                            <div className="form-group">
                                <label className="control-label">Ngày tham gia</label>
                                <input type="text" className="form-control" defaultValue={user.created_at} readOnly />
                            </div>
                            <div className="form-group">
                                <label className="control-label">Facebook ID</label>
                                <div className="input-group mar-btm">
                                    <input type="text" className="form-control" placeholder="facebook ID" onChange={e=>{setFbId(e.target.value)}} defaultValue={fbId} />
                                    <div className="input-group-btn">
                                        <button className="btn btn-primary" id="btn-update-fb-id" onClick={handleUpdateFbId} type="button" aria-expanded="false">
                                            <i className="fa fa-edit" /> Sửa ID
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </Grid>
                    </GridContainer>
                </Box>
            </Paper>
        </div>
    );

}

export default AccountInfo;