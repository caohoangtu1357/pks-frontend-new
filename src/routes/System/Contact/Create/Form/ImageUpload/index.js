import { Button } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { CloudUpload } from "@material-ui/icons";
import './index.scss';

function ImageUpload(props) {

    const [selectedFile, setSelectedFile] = useState();
    const [preview, setPreview] = useState();

    useEffect(() => {
        if (!selectedFile) {
            setPreview(undefined)
            return
        }

        const objectUrl = URL.createObjectURL(selectedFile)
        setPreview(objectUrl)

        return () => URL.revokeObjectURL(objectUrl)
    }, [selectedFile])

    const onSelectFile = e => {
        if (!e.target.files || e.target.files.length === 0) {
            setSelectedFile(undefined)
            return
        }
        props.setImage(e.target.files[0]);
        setSelectedFile(e.target.files[0])
    }

    return (
        <div className="upload-image-control">
            <div
                className="form-group"
                id="img-preview-wrapper"
            >
                <div style={{ width: "100%", textAlign: "center" }}>
                    {selectedFile && <img style={{ maxWidth: 250 }} src={preview} />}
                </div>
            </div>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-12">
                        <div className="btn btn-primary input-image-wrapper">
                            <span>Thêm hình ảnh <CloudUpload /></span>
                            <input
                                onChange={onSelectFile}
                                id="image"
                                name="image"
                                type="file"
                                className="input-image"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ImageUpload;