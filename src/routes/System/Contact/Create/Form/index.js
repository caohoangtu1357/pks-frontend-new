import React, { useState } from "react";
import { Typography } from "@material-ui/core";
import "./index.scss";
import { makeStyles } from "@material-ui/styles";
import { useDispatch } from "react-redux";
import { createContact } from '../../../../../redux/actions';
import { useForm } from "react-hook-form";
import { contactProblems } from '@custom/constants/System';
import ImageUpload from "./ImageUpload";
import { Send } from "@material-ui/icons";

const useStyles = makeStyles({
    infoAlert: {
        "& .MuiAlert-icon": {
            display: "none"
        }
    }
});

function Select(props) {
    return (
        <>
            <select 
                {...props.register("problem", { required: true, min: 1 })}
                name="problem" className={props.className}
                value={props.problem}
                onChange={e => props.setProblem(e.target.value)}
            >
                {props.data.map((item, index) => {
                    if (item.value == props.selected) {
                        return <option key={index} value={item.value} selected>{item.label}</option>
                    }
                    return <option key={index} value={item.value}>{item.label}</option>
                })}
            </select>
            {props.errors.problem?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
        </>
    )
}

const Form = () => {

    const [problem, setProblem] = useState("");
    const [title, setTitle] = useState("");
    const [contactId, setContactId] = useState("");
    const [description, setDescription] = useState("");
    const [image, setImage] = useState("");

    const dispatch = useDispatch();
    const { register, handleSubmit, formState: { errors }, reset } = useForm();

    function handleCreate(data, e) {
        dispatch(createContact({
            problem: data.problem,
            title: data.title,
            contact_id: data.contactId,
            description: data.description,
            image: image
        }));
    }

    const classes = useStyles();
    return (
        <div className="buff-comment-form">
            <Typography className="heading" variant="h3">Thông tin hỗ trợ</Typography>
            <form onSubmit={handleSubmit(handleCreate)}>
                <div className="form-group">
                    <label>Vấn đề cần hỗ trợ</label>
                    <Select
                        problem={problem}
                        setProblem={setProblem}
                        errors={errors}
                        register={register}
                        data={contactProblems}
                        className="form-control"
                    />
                </div>
                <div className="form-group">
                    <label>Tiêu đề hỗ trợ</label>
                    <input
                        {...register("title", { required: true })}
                        type="text"
                        value={title}
                        onChange={e => setTitle(e.target.value)}
                        className="form-control"
                    />
                    {errors.title?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                </div>
                <div className="form-group">
                    <label>Id nếu có</label>
                    <input
                        {...register("contactId", { required: true })}
                        type="text"
                        value={contactId}
                        onChange={e => setContactId(e.target.value)}
                        className="form-control"
                    />
                    {errors.contactId?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                </div>
                <div className="form-group">
                    <label>Mô tả chi tiết</label>
                    <textarea
                        {...register("description", { required: true })}
                        type="text"
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                        className="form-control"
                        placeholder="mô tả chi tiết."
                        rows={8}
                    ></textarea>
                    {errors.description?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                </div>
                <ImageUpload setImage={setImage}/>
                <div className="btn-purchase">
                    <button className="btn btn-success" type="submit">
                        Gửi hỗ trợ <Send/>
                    </button>
                </div>
            </form>
        </div>
    )
}

export default Form;