import React from "react";
import { useDispatch } from "react-redux";
import { deleteContact } from '../../../../../redux/actions';
import Swal from "@custom/config/Swal";
import { Link } from "react-router-dom";
import { Delete, DeleteForeverRounded, EditOutlined } from "@material-ui/icons";

export default function ActionColumn(props) {

    const dispatch = useDispatch();

    function deleteBtn(id) {
        Swal({
            title: "Xóa",
            text: "Xác nhận xóa liên hệ hỗ trợ",
            icon: "warning",
            hadleAction: () => {
                dispatch(deleteContact(id));
            }
        });
    }

    return (
        <div key={props.id}>
            <Link
                style={{ padding: "0px 2px" }}
                to={"/system/contact/edit/" + props.id}
                className="add-tooltip btn btn-primary btn-xs"
                data-placement="top"
                title="Chỉnh sửa Liên Hệ Hỗ Trợ"
            >
                <EditOutlined style={{ fontSize: 20 }} />
            </Link>
            <button
                onClick={(e) => deleteBtn(e.currentTarget.value)}
                style={{ marginLeft: 5, padding: "0px 2px" }}
                value={props.id}
                className="add-tooltip btn btn-danger btn-xs cancel-btn"
                data-placement="top"
                title="Xoá liên hệ hỗ trợ"
            >
                <Delete style={{ fontSize: 20 }} />
            </button>
        </div>
    )
}
