import React from "react";
import { CONTACT_STATUS } from '@custom/constants/System';
import { makeStyles } from "@material-ui/styles";

const styles = makeStyles({
    labelWaitting: {
        backgroundColor: "#2196f3",
        color: "white",
        padding: "5px 12px",
        borderRadius: 5
    },
    labelProcessing: {
        backgroundColor: "#ffc107",
        color: "white",
        padding: "5px 10px",
        borderRadius: 5
    },
    labelProcessed: {
        backgroundColor: "#8bc34a",
        color: "white",
        padding: "5px 17px",
        borderRadius: 5
    },
});

export default function StatusColumn(props) {
    const classes = styles();
    switch (props.status){
        case 0:
            return <span className={classes.labelWaitting}>{CONTACT_STATUS[1].label}</span>
        case 1:
            return <span className={classes.labelProcessing}>{CONTACT_STATUS[2].label}</span>
        case 2:
            return <span className={classes.labelProcessed}>{CONTACT_STATUS[3].label}</span>
    }
}