import GridContainer from '@jumbo/components/GridContainer';
import { Box, Button, FormControl, FormHelperText, Grid, InputLabel, NativeSelect, Select } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import { makeStyles } from '@material-ui/styles';
import React, { useState, useEffect } from 'react';
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from 'react-bootstrap-table2-paginator';
import { useDispatch, useSelector } from 'react-redux';
import { getAllContact } from '../../../../redux/actions';
import { Link } from 'react-router-dom';
import ActionColumn from './ColumnFormatter/Action';
import StatusColumn from './ColumnFormatter/Status';
import { CONTACT_STATUS } from '@custom/constants/System';

const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(2),
        minWidth: 120,
        verticalAlign: "middle"
    },
    selectEmpty: {
        marginTop: theme.spacing(4),
    },
}));

function ContactTable(props) {

    const dispatch = useDispatch();

    const { contact, allContact } = useSelector(({ contact }) => contact);

    const classes = useStyles();

    const [page, setPage] = useState(1);
    const [sizePerPage, setSizePerPage] = useState(10);
    const [sort, setSort] = useState('created_at');
    const [order, setOrder] = useState('desc');
    const [status, setStatus] = useState(null);

    useEffect(() => {
        dispatch(getAllContact({
            limit: sizePerPage,
            offset: 0,
            sort: sort,
            order: order,
        }));
    }, [contact])

    function onTableChange(type, newState) {
        let sortField = newState.sortField?newState.sortField:sort;
        dispatch(getAllContact({
            sort: sortField,
            order: newState.sortOrder,
            limit: newState.sizePerPage,
            offset: (newState.page - 1) * newState.sizePerPage,
            status:parseInt(status)
        }));
        setSort(sortField);
        setOrder(newState.sortOrder);
        setPage(newState.page);
        setSizePerPage(newState.sizePerPage);
    }

    function applyFilter(statusStr) {
        dispatch(getAllContact({
            sort: sort,
            order: order,
            limit: sizePerPage,
            offset: 0,
            status: parseInt(statusStr)
        }));
        setStatus(statusStr);
    }

    const paginateOptions = {
        page: page,
        sizePerPage: sizePerPage,
        showTotal: true,
        totalSize: allContact.length != 0 ? allContact.total : 0,
        hidePageListOnlyOnePage: true,
        paginationTotalRenderer: (from, to, size) => {
            return (
                <span className="react-bootstrap-table-pagination-total"> Hiển thị từ {from} đến {to} của {size}</span>
            )
        }
    };

    const columns = [{
        dataField: 'title',
        text: 'Tiêu đề',
        sort: true
    }, {
        dataField: 'problem',
        text: 'Vấn đề',
        sort: true
    }, {
        dataField: 'status',
        text: 'Trạng thái',
        sort: true,
        headerAlign: 'center',
        align: "center",
        formatter: (cell, row, index) => (<StatusColumn status={row.status} />)
    }, {
        dataField: '_id',
        text: 'Chức năng',
        sort: true,
        headerAlign: 'center',
        align: "center",
        formatter: (cell, row, index) => (<ActionColumn id={row._id} />)
    }];

    return (
        <Box padding={3}>
            <GridContainer>
                <Grid item sm={12}>
                    <Box textAlign="end" marginBottom="10px">

                        <FormControl className={classes.formControl}>
                            <NativeSelect
                                value={status}
                                onChange={e => { applyFilter(e.target.value); }}
                                name="age"
                                className={classes.selectEmpty}
                                inputProps={{ 'aria-label': 'age' }}>
                                {
                                    CONTACT_STATUS.map((itm, index) => (
                                        <option value={itm.value} key={index}>{itm.label}</option>
                                    ))
                                }
                            </NativeSelect>
                            <FormHelperText>Lọc theo trạng thái</FormHelperText>
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <Link to="/system/contact/create">
                                <Button variant="contained" color="primary" startIcon={<Add />}>Tạo hỗ trợ</Button>
                            </Link>
                        </FormControl>
                    </Box>
                </Grid>
                <Grid item sm={12}>
                    <BootstrapTable
                        onTableChange={onTableChange}
                        keyField='_id'
                        remote={true}
                        data={allContact.length != 0 ? allContact.rows : []}
                        columns={columns}
                        pagination={paginationFactory(paginateOptions)}
                        keyBoardNav
                    />
                </Grid>
            </GridContainer>
        </Box>

    )
}

export default ContactTable;