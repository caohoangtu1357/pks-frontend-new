import GridContainer from '@jumbo/components/GridContainer';
import PageContainer from '@jumbo/components/PageComponents/layouts/PageContainer';
import { Grid, Paper } from '@material-ui/core';
import React from 'react';
import UpdateForm from './Form';
import Info from '../Info';

const breadcrumbs = [
    { label: 'Hệ thống', link: '#' },
    { label: 'Liên hệ hỗ trợ', link: '/system/contact' },
];

const Create = () => {
    return (
        <PageContainer className="contact" heading="Liên hệ hỗ trợ" breadcrumbs={breadcrumbs}>
            <GridContainer>
                <Grid item sm={8}>
                    <Paper className="paper-wrapper">
                        <UpdateForm />
                    </Paper>
                </Grid>
                <Grid item sm={4}>
                    <Paper className="paper-wrapper">
                        <Info />
                    </Paper>
                </Grid>
            </GridContainer>
        </PageContainer>
    )
}

export default Create;