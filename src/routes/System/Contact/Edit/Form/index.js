import React, { useEffect, useState } from "react";
import { Typography } from "@material-ui/core";
import "./index.scss";
import { makeStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import { getDetailContact, updateContact } from '../../../../../redux/actions';
import { useForm } from "react-hook-form";
import { contactProblems } from '@custom/constants/System';
import ImageUpload from "./ImageUpload";
import { Send } from "@material-ui/icons";
import { useParams } from "react-router";

const useStyles = makeStyles({
    infoAlert: {
        "& .MuiAlert-icon": {
            display: "none"
        }
    }
});

function Select(props) {
    console.log(props.selected);
    return (
        <>
            <select
                {...props.register("problem", { required: true })}
                name="problem"
                className={props.className}
                value={props.problem}
            >
                {props.data.map((item, index) => {
                    if (item.value == props.selected) {
                        return <option key={index} value={item.value} selected>{item.label}</option>
                    }
                    return <option key={index} value={item.value}>{item.label}</option>
                })}
            </select>
            {props.errors.problem?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
        </>
    )
}

const UpdateForm = () => {

    const [image, setImage] = useState("");
    const { contact } = useSelector(({ contact }) => contact);
    const id = useParams().id;

    const dispatch = useDispatch();
    const { register, handleSubmit, formState: { errors }, setValue } = useForm();

    setValue("title", contact?.title);
    setValue("problem", contact?.problem);
    setValue("description", contact?.description);
    setValue("contact_id", contact?.contact_id);

    function handleCreate(data, e) {
        dispatch(updateContact({
            problem: data.problem,
            title: data.title,
            contact_id: data.contact_id,
            description: data.description,
            image: image
        }, id));
    }

    useEffect(() => {
        dispatch(getDetailContact(id));
    }, []);

    const classes = useStyles();
    return (
        <div className="buff-comment-form">
            <Typography className="heading" variant="h3">Thông tin hỗ trợ</Typography>
            <form onSubmit={handleSubmit(handleCreate)}>
                <div className="form-group">
                    <label>Vấn đề cần hỗ trợ</label>
                    <Select
                        selected={contact?.problem}
                        errors={errors}
                        register={register}
                        data={contactProblems}
                        className="form-control"
                    />
                </div>
                <div className="form-group">
                    <label>Tiêu đề hỗ trợ</label>
                    <input
                        {...register("title", { required: true })}
                        type="text"
                        className="form-control"
                    />
                    {errors.title?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                </div>
                <div className="form-group">
                    <label>Id nếu có</label>
                    <input
                        {...register("contact_id", { required: true })}
                        type="text"
                        className="form-control"
                    />
                    {errors.contactId?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                </div>
                <div className="form-group">
                    <label>Mô tả chi tiết</label>
                    <textarea
                        {...register("description", { required: true })}
                        type="text"
                        className="form-control"
                        placeholder="mô tả chi tiết."
                        rows={8}
                    ></textarea>
                    {errors.description?.type === "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                </div>
                <ImageUpload setImage={setImage} oldImage={contact?.image_path} />
                <div className="btn-purchase">
                    <button className="btn btn-success" type="submit">
                        Gửi hỗ trợ <Send />
                    </button>
                </div>
            </form>
        </div>
    )
}

export default UpdateForm;