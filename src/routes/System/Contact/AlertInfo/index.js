import React from 'react';
import { Alert } from '@material-ui/lab';

const AlertInfo = () => {
    return (
        <Alert style={{backgroundColor:"#ccf6e4"}}>
            Lưu ý!<br/>
            - Hỗ trợ trong giờ hành chính từ Thứ 2 đến Thứ 6.<br/>
            - Giờ làm việc từ 9h30 sáng đến 18h30 chiều.<br/>
            - Ngoài giờ làm việc sẽ hỗ trợ chậm hơn và phụ thuộc nhân viên hỗ trợ Online.<br/>
            - Nếu vấn đề không cần gấp vui lòng chờ đến giờ làm việc để xử lý tốt nhất, nhường cho các bạn cần hỗ trợ gấp ngoài giờ.
        </Alert>
    )
}

export default AlertInfo;