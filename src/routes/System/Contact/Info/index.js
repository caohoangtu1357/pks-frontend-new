import React from "react";
import { Alert } from "@material-ui/lab";
import { makeStyles } from "@material-ui/styles";
// import "./index.scss";

const useStyles = makeStyles(theme => ({
    infoAlert: {
        "& .MuiAlert-icon": {
            display: "none"
        }
    }
}));

const Info = () => {
    const classes = useStyles();
    return (
        <div>
            <div className="info-item">
                <Alert className variant="filled" severity="info" style={{ justifyContent: "center" }} className={classes.infoAlert}>
                Quan trọng!<br/>
                - Hỗ trợ trong giờ hành chính từ Thứ 2 đến Thứ 6.<br/>
                - Giờ làm việc từ 9h30 sáng đến 18h30 chiều.<br/>
                - Ngoài giờ làm việc sẽ hỗ trợ chậm hơn và phụ thuộc nhân viên hỗ trợ Online.<br/>
                - Nếu vấn đề không cần gấp vui lòng chờ đến giờ làm việc để xử lý tốt nhất, nhường cho các bạn cần hỗ trợ gấp ngoài giờ.
                </Alert>
            </div>
            <div className="info-item">
                <Alert variant="filled" severity="error" style={{ justifyContent: "center" }} className={classes.infoAlert}>
                Lưu ý!<br/>
                - Hỗ trợ trong giờ hành chính từ Thứ 2 đến Thứ 6.<br/>
                - Giờ làm việc từ 9h30 sáng đến 18h30 chiều.<br/>
                - Ngoài giờ làm việc sẽ hỗ trợ chậm hơn và phụ thuộc nhân viên hỗ trợ Online.<br/>
                - Nếu vấn đề không cần gấp vui lòng chờ đến giờ làm việc để xử lý tốt nhất, nhường cho các bạn cần hỗ trợ gấp ngoài giờ.
                </Alert>
            </div>
        </div>
    )
}

export default Info;