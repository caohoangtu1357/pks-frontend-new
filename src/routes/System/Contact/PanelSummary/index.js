import React, { useEffect } from 'react';
import HoverInfoCard from '@jumbo/components/Common/HoverInfoCard';
import GridContainer from '@jumbo/components/GridContainer';
import { Box, Grid, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { Cached, History } from '@material-ui/icons';
import CheckCircleOutline from '@material-ui/icons/CheckCircleOutline';
import {statiscticContact} from '../../../../redux/actions';

const styles = makeStyles(theme=>({
    summaryCard:{
        margin:"5px 12px 5px 12px"
    }
}));

const PanelSummary = () => {
    const classes = styles();
    const {statistic,contact} = useSelector(({contact})=>contact);
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch(statiscticContact());
    },[contact])

    return (
        <GridContainer >
            <Grid md={4} item className="summary-item">
                <Box className={classes.summaryCard}>
                    <HoverInfoCard
                        className={classes.summaryCard}
                        icon={<History style={{ color: '#ffffff' }} />}
                        backgroundColor="#0795F4"
                        title={statistic.waiting}
                        subTitle="Chờ hỗ trợ"
                    />
                </Box>
            </Grid>
            <Grid md={4} item className="summary-item">
                <Box className={classes.summaryCard}>
                    <HoverInfoCard
                        className={classes.summaryCard}
                        icon={<Cached style={{ color: '#ffffff' }} />}
                        backgroundColor="#ffa31c"
                        title={statistic.processing}
                        subTitle="Đang hỗ trợ"
                    />
                </Box>
            </Grid>
            <Grid md={4} item className="summary-item">
                <Box className={classes.summaryCard}>
                    <HoverInfoCard
                        className={classes.summaryCard}
                        icon={<CheckCircleOutline style={{ color: '#ffffff' }} />}
                        backgroundColor="#8bc34a"
                        title={statistic.processed}
                        subTitle="Đã hỗ trợ"
                    />
                </Box>
            </Grid>
        </GridContainer>
    )
}

export default PanelSummary;