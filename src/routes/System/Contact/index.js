import GridContainer from '@jumbo/components/GridContainer';
import PageContainer from '@jumbo/components/PageComponents/layouts/PageContainer';
import { Grid, Paper } from '@material-ui/core';
import React from 'react';
import ContactTable from './ContactTable';
import PanelSummary from './PanelSummary';
import AlertInfo from './AlertInfo';
import './index.scss';

const breadcrumbs = [
    { label: 'Hệ thống', link: '#' },
    { label: 'Liên hệ hỗ trợ', link: '/system/contact' },
];

const Contact = () => {
    return (
        <PageContainer className="contact" heading="Liên hệ hỗ trợ" breadcrumbs={breadcrumbs}>
            <GridContainer>
                <Paper className="paper-wrapper">
                    <Grid item sm={12} className="alert-info-wrapper">
                        <AlertInfo />
                    </Grid>
                    <Grid item sm={12} className="panel-summary">
                        <PanelSummary />
                    </Grid>
                    <Grid item sm={12}>
                        <ContactTable />
                    </Grid>
                </Paper>
            </GridContainer>
        </PageContainer>
    )
}

export default Contact;