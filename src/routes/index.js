import React from 'react';
import { Redirect, Route, Switch } from 'react-router';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import SamplePage from './Pages/SamplePage';
import Error404 from './Pages/404';
import Login from './Auth/Login';
import Register from './Auth/Register';
import VerifyAccont from './Auth/VerifyAccount';
import ForgotPasswordPage from './Auth/ForgotPassword';
import VerifyResetPassword from './Auth/VerifyResetPassword';
import System from './System';
import Facebook from './Facebook';

const RestrictedRoute = ({ component: Component, ...rest }) => {
  const { user } = useSelector(({ user }) => user);
  return (
    <Route
      {...rest}
      render={props =>
        user ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/signin',
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};

const Routes = () => {
  const { user } = useSelector(({ user }) => user);
  const location = useLocation();

  if (location.pathname === '' || location.pathname === '/') {
    return <Redirect to={'/system'} />;
  } else if (user && location.pathname === '/signin') {
    return <Redirect to={'/system'} />;
  }

  return (
    <React.Fragment>
      <Switch>
        <RestrictedRoute component={System} path="/system"/>
        <RestrictedRoute component={Facebook} path="/facebook"/>
        <RestrictedRoute component={SamplePage} path="/sample-page"/>
        <Route path="/signin" component={Login} />
        <Route path="/signup" component={Register} />
        <Route path="/forgot-password" component={ForgotPasswordPage} />
        <Route path="/verify-account" component={VerifyAccont} />
        <Route path="/verify-reset-password" component={VerifyResetPassword} />
        <RestrictedRoute component={Error404} />
      </Switch>
    </React.Fragment>
  );
};

export default Routes;
