import { CLEAN_POSTS } from "@custom/constants/ActionTypes/Notify";
import {
    LOAD_MORE_POST_SUCCESS
} from "@custom/constants/ActionTypes/Post";

const init = {
    data: null,
    posts: []
}

export default function PostReducer(state = init, action) {
    switch (action.type) {
        case LOAD_MORE_POST_SUCCESS:
            return {
                ...state,
                posts: action.data
            }
        case CLEAN_POSTS:
            return {
                ...state,
                posts: []
            }
        default:
            return state;
    }
}