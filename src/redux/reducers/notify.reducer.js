import {
    LOAD_NOTIFY_SUCCESS
} from "@custom/constants/ActionTypes/Notify";

const init = {
    data: null,
    notifies:[]
}

export default function NotifyReducer(state = init, action) {
    switch (action.type) {
        case LOAD_NOTIFY_SUCCESS:
            return {
                ...state,
                notifies:action.data
            }
        default:
            return state;
    }
}