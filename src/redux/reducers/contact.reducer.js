import {
    CREATE_CONTACT_SUCCESS,
    GET_CONTACT_ALL_SUCCESS,
    GET_CONTACT_DETAIL_SUCCESS,
    UPDATE_CONTACT_SUCCESS,
    STATISTIC_CONTACT_SUCCESS,
    DELETE_CONTACT_SUCCESS
} from "@custom/constants/ActionTypes/Contact";

const init = {
    contact: {},
    allContact:[],
    statistic:{
        waiting:0,
        processing:0,
        processed:0   
    }
}

export default function ContactReducer(state = init, action) {
    switch (action.type) {
        case CREATE_CONTACT_SUCCESS:
            return {
                ...state,
                contact:{}
            }
        case DELETE_CONTACT_SUCCESS:{
            return {
                ...state,
                contact:{}
            }
        }
        case GET_CONTACT_ALL_SUCCESS:
            return {
                ...state,
                allContact:action.data
            }
        case GET_CONTACT_DETAIL_SUCCESS:
            return{
                ...state,
                contact:action.data
            }
        case UPDATE_CONTACT_SUCCESS:
            return{
                ...state,
                contact:{}
            }
        case STATISTIC_CONTACT_SUCCESS:
            return{
                ...state,
                statistic:{
                    waiting:action.data.total.WAITING,
                    processing:action.data.total.PROCESSING,
                    processed:action.data.total.PROCESSED  
                }
            }
        default:
            return state;
    }
}