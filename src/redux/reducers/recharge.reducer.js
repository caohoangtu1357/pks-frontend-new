import {
    GET_RECHARGE_SUCCESS,
    GET_RECHARGE_ALL_SUCCESS
} from "@custom/constants/ActionTypes/Recharge";

const init = {
    recharge: {
        total:0,
        total_amount_current_month:0
    },
    allRecharge:[]
}

export default function RechargeRuducer(state = init, action) {
    switch (action.type) {
        case GET_RECHARGE_SUCCESS:
            return {
                ...state,
                recharge:action.data
            }
        case GET_RECHARGE_ALL_SUCCESS:
            return {
                ...state,
                allRecharge:action.data
            }
        default:
            return state;
    }
}