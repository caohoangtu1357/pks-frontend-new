import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import Common from './Common';
import WallApp from './WallApp';
import AlertReducer from './alert.reducer';
import AuthReducer from './auth.reducer';
import BuffCommentReducer from './buff-comment.reducer';
import BuffLikeReducer from './buff-like.reducer';
import BuffShareReducer from './buff-share.reducer';
import ContactReducer from './contact.reducer';
import NotifyReducer from './notify.reducer';
import OrderReducer from './order.reducer';
import PageTitleReducer from './page-title.reducer';
import PostReducer from './post.reducer';
import RechargeRuducer from './recharge.reducer';
import UserReducer from './user.reducer';
import BuffLivestreamReducer from './buff-livestream.reducer';

export default history =>
  combineReducers({
    router: connectRouter(history),
    common: Common,
    wallApp: WallApp,
    alert: AlertReducer,
    auth: AuthReducer,
    buffComment: BuffCommentReducer,
    buffLike: BuffLikeReducer,
    buffShare: BuffShareReducer,
    buffLivestream:BuffLivestreamReducer,
    contact: ContactReducer,
    notify: NotifyReducer,
    order: OrderReducer,
    pageTitle: PageTitleReducer,
    post: PostReducer,
    recharge: RechargeRuducer,
    user: UserReducer
  });
