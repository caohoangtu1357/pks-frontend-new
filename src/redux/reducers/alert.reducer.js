import {
    SHOW_ALERT,
    TURN_OFF_ALERT
} from "@custom/constants/ActionTypes/Alert";

const init = {
    data: {
        display: "none",
        variant: "danger",
        content:""
    }
}

export default function AlertReducer(state = init, action) {
    switch (action.type) {
        case SHOW_ALERT:
            return {
                data: {
                    variant:action.data.variant,
                    content:action.data.content,
                    display:"unset"
                }
            }
        case TURN_OFF_ALERT:
            return {
                ...state,
                data: {
                    display: "none"
                }
            }
        default:
            return state;
    }
}