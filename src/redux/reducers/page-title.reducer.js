import { SET_PAGE_TITLE } from "@custom/constants/ActionTypes/PageTitle";

const init = {
    main: null,
    breadcrumb_1:null,
    breadcrumb_2:null
}

export default function PageTitleReducer(state = init, action) {
    switch (action.type) {
        case SET_PAGE_TITLE:
            return {
                main: action.data.main,
                breadcrumb_1:action.data.breadcrumb_1,
                breadcrumb_2:action.data.breadcrumb_2
            }
        default:
            return state;
    }
}