import {
    CREATE_BUFF_COMMENT_SUCCESS,
    GET_BUFF_COMMENT_ALL_SUCCESS
} from "@custom/constants/ActionTypes/BuffComment";

const init = {
    buffComment: {
        post_id:"",
        number:10,
        comments:"",
        note:""
    },
    allBuffComment:[]
}

export default function BuffCommentReducer(state = init, action) {
    switch (action.type) {
        case CREATE_BUFF_COMMENT_SUCCESS:
            return {
                ...state,
                buffComment:{
                    post_id:"",
                    number:10,
                    comments:"",
                    note:""
                }
            }
        case GET_BUFF_COMMENT_ALL_SUCCESS:
            return {
                ...state,
                allBuffComment:action.data
            }
        default:
            return state;
    }
}