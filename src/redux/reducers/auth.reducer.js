import {
    LOGIN_SUCCESS
} from "@custom/constants/ActionTypes/Auth";

const init = {
    user: null,
    loadUser:true
};

export default function AuthReducer(state = init, action) {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return {
                ...state,
                user: action.data
            };
        default:
            return state;
    }
}