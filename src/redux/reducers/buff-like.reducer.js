import {
    CREATE_BUFF_LIKE_SUCCESS,
    GET_BUFF_LIKE_ALL_SUCCESS
} from "@custom/constants/ActionTypes/BuffLike";

const init = {
    buffLike: {
        post_id:"",
        number:10,
        note:""
    },
    allBuffLike:[]
}

export default function BuffLikeReducer(state = init, action) {
    switch (action.type) {
        case CREATE_BUFF_LIKE_SUCCESS:
            return {
                ...state,
                buffLike:{
                    post_id:"",
                    number:10,
                    note:""
                }
            }
        case GET_BUFF_LIKE_ALL_SUCCESS:
            return {
                ...state,
                allBuffLike:action.data
            }
        default:
            return state;
    }
}