import {
    CREATE_BUFF_SHARE_SUCCESS,
    GET_BUFF_SHARE_ALL_SUCCESS
} from "@custom/constants/ActionTypes/BuffShare";

const init = {
    buffShare: {
        post_id:"",
        number:10,
        note:""
    },
    allBuffShare:[]
}

export default function BuffShareReducer(state = init, action) {
    switch (action.type) {
        case CREATE_BUFF_SHARE_SUCCESS:
            return {
                ...state,
                buffShare: {
                    post_id:"",
                    number:10,
                    note:""
                }
            }
        case GET_BUFF_SHARE_ALL_SUCCESS:
            return {
                ...state,
                allBuffShare:action.data
            }
        default:
            return state;
    }
}