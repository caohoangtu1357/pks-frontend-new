import {
    GET_INFO_SUCCESS,
    GET_INFO_FAIL
} from "@custom/constants/ActionTypes/User";
 
 const init = {
     user:null
 };
 
 export default function UserReducer(state= init, action){
     switch (action.type){
        case GET_INFO_SUCCESS:
            return {
                ...state,
                user:action.data
            };
        case GET_INFO_FAIL:
            return {
                ...state,
                user:null
            }
         default:
             return state;
     }
 }