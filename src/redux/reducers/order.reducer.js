import {
    GET_ORDER_ALL_SUCCESS
} from "@custom/constants/ActionTypes/Order";

const init = {
    order: null,
    allOrder:[]
}

export default function OrderReducer(state = init, action) {
    switch (action.type) {
        case GET_ORDER_ALL_SUCCESS:
            return {
                ...state,
                allOrder:action.data
            }
        default:
            return state;
    }
}