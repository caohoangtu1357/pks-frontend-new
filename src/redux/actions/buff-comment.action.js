import {
    GET_BUFF_COMMENT_ALL_SUCCESS,
    CREATE_BUFF_COMMENT_SUCCESS,
    GET_BUFF_COMMENT_ALL,
    CREATE_BUFF_COMMENT
} from "@custom/constants/ActionTypes/BuffComment";

export function getBuffCommentAllSuccess(data) {
    return { type: GET_BUFF_COMMENT_ALL_SUCCESS, data: data }
}

export function createBuffCommentSuccess() {
    return { type: CREATE_BUFF_COMMENT_SUCCESS }
}

export function getBuffCommentAll(filters) {
    return { type: GET_BUFF_COMMENT_ALL, filters }
}

export function createBuffComment(attributes) {
    return { type: CREATE_BUFF_COMMENT, attributes }
}