import {
    LOGIN_SUCCESS,
    LOGOUT_SUCCESS,
    LOGIN,
    REGISTER,
    VERIFY_ACCOUNT,
    LOGOUT,
    RESET_PASSWORD,
    VERIFY_RESET_PASSWORD
} from "@custom/constants/ActionTypes/Auth";

export function logginSuccess(data) {
    return { type: LOGIN_SUCCESS, data: data }
}

export function loggoutSuccess() {
    return { type: LOGOUT_SUCCESS }
}

export function login(credentials) {
    return { type: LOGIN, credentials }
}

export function register(credentials) {
    return { type: REGISTER, credentials }
}

export function verifyAccount(credentials) {
    return { type: VERIFY_ACCOUNT, credentials }
}

export function logout() {
    return { type: LOGOUT }
}

export function resetPassword(credentials) {
    return { type: RESET_PASSWORD, credentials }
}

export function verifyResetPassword(credentials) {
    return { type: VERIFY_RESET_PASSWORD, credentials }
}