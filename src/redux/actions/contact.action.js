import {
    GET_CONTACT_SUCCESS,
    DELETE_CONTACT_SUCCESS,
    CREATE_CONTACT_SUCCESS,
    GET_CONTACT_ALL_SUCCESS,
    GET_CONTACT_DETAIL_SUCCESS,
    UPDATE_CONTACT_SUCCESS,
    STATISTIC_CONTACT_SUCCESS,
    GET_CONTACT_ALL,
    GET_CONTACT,
    STATISTIC_CONTACT,
    CREATE_CONTACT,
    DELETE_CONTACT,
    GET_CONTACT_DETAIL,
    UPDATE_CONTACT
} from "@custom/constants/ActionTypes/Contact";

export function createContactSuccess(data) {
    return { type: CREATE_CONTACT_SUCCESS, data: data }
}

export function createContact(contactInfo) {
    return { type: CREATE_CONTACT, contactInfo }
}

export function deleteContactSuccess() {
    return { type: DELETE_CONTACT_SUCCESS }
}

export function deleteContact(id) {
    return { type: DELETE_CONTACT, id }
}

export function getContactSuccess() {
    return { type: GET_CONTACT_SUCCESS }
}

export function getContactAllSuccess(data) {
    return { type: GET_CONTACT_ALL_SUCCESS, data }
}

export function getContact() {
    return { type: GET_CONTACT }
}

export function getAllContact(filters) {
    return { type: GET_CONTACT_ALL, filters }
}

export function getDetailContactSuccess(data) {
    return { type: GET_CONTACT_DETAIL_SUCCESS, data }
}

export function getDetailContact(id) {
    return { type: GET_CONTACT_DETAIL, id }
}

export function updateContactSuccess() {
    return { type: UPDATE_CONTACT_SUCCESS }
}

export function updateContact(contactInfo, id) {
    return { type: UPDATE_CONTACT, contactInfo, id }
}

export function statiscticContactSuccess(data) {
    return { type: STATISTIC_CONTACT_SUCCESS, data }
}

export function statiscticContact() {
    return { type: STATISTIC_CONTACT }
}