import {
    GET_BUFF_LIKE_ALL_SUCCESS,
    CREATE_BUFF_LIKE_SUCCESS,
    GET_BUFF_LIKE_ALL,
    CREATE_BUFF_LIKE
} from "@custom/constants/ActionTypes/BuffLike";

export function getBuffLikeAllSuccess(data) {
    return { type: GET_BUFF_LIKE_ALL_SUCCESS, data: data }
}

export function createBuffLikeSuccess() {
    return { type: CREATE_BUFF_LIKE_SUCCESS }
}

export function getBuffLikeAll(filters) {
    return { type: GET_BUFF_LIKE_ALL, filters }
}

export function createBuffLike(attributes) {
    return { type: CREATE_BUFF_LIKE, attributes }
}