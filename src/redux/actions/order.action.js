import {
    GET_ORDER_ALL_SUCCESS,
    GET_ORDER_ALL
} from "@custom/constants/ActionTypes/Order";

export function getOrderAllSuccess(data){
    return {type:GET_ORDER_ALL_SUCCESS,data:data}
}

export function getAllOrder(filters){
    return {type:GET_ORDER_ALL,filters}
}