import {
    LOAD_NOTIFY_SUCCESS,
    LOAD_NOTIFY
} from "@custom/constants/ActionTypes/Notify";

export function getNotifySuccess(data) {
    return { type: LOAD_NOTIFY_SUCCESS, data }
}

export function getNotify() {
    return { type: LOAD_NOTIFY }
}