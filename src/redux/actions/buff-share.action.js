import {
    GET_BUFF_SHARE_ALL_SUCCESS,
    CREATE_BUFF_SHARE_SUCCESS,
    GET_BUFF_SHARE_ALL,
    CREATE_BUFF_SHARE
} from "@custom/constants/ActionTypes/BuffShare";

export function getBuffShareAllSuccess(data) {
    return { type: GET_BUFF_SHARE_ALL_SUCCESS, data: data }
}

export function createBuffShareSuccess() {
    return { type: CREATE_BUFF_SHARE_SUCCESS }
}

export function getBuffShareAll(filters) {
    return { type: GET_BUFF_SHARE_ALL, filters }
}

export function createBuffShare(attributes) {
    return { type: CREATE_BUFF_SHARE, attributes }
}