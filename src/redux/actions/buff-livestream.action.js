import {
    GET_BUFF_LIVE_STREAM_ALL_SUCCESS,
    CREATE_BUFF_LIVE_STREAM_SUCCESS,
    CREATE_BUFF_LIVE_STREAM,
    GET_BUFF_LIVE_STREAM_ALL
} from "@custom/constants/ActionTypes/BuffLiveStream";

export function getBuffLiveStreamAllSuccess(data) {
    return { type: GET_BUFF_LIVE_STREAM_ALL_SUCCESS, data: data }
}

export function createBuffLiveStreamSuccess() {
    return { type: CREATE_BUFF_LIVE_STREAM_SUCCESS }
}

export function getBuffLiveStreamAll(filters) {
    return { type: GET_BUFF_LIVE_STREAM_ALL, filters }
}

export function createBuffLivestream(attributes) {
    return { type: CREATE_BUFF_LIVE_STREAM, attributes }
}