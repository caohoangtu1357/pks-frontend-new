import {
    LOAD_MORE_POST_SUCCESS,
    LOAD_MORE_POST
} from "@custom/constants/ActionTypes/Post";

export function loadMorePostSuccess(data) {
    return { type: LOAD_MORE_POST_SUCCESS, data }
}

export function loadMorePost(filters) {
    return { type: LOAD_MORE_POST, filters }
}