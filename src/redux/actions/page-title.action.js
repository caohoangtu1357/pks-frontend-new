import {
    SET_PAGE_TITLE
} from "@custom/constants/ActionTypes/PageTitle";

export function setPageTitle(main, breadcrumb_1=null, breadcrumb_2=null) {
    document.title = main;
    return { type: SET_PAGE_TITLE, data: { main: main, breadcrumb_1: breadcrumb_1, breadcrumb_2: breadcrumb_2 } }
}