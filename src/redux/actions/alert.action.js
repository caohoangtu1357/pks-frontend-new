import {SHOW_ALERT, TURN_OFF_ALERT} from "@custom/constants/ActionTypes/Alert";

export function showAlert(data){
    return {type:SHOW_ALERT,data:data}
}

export function turnOffAlert(){
    return {type:TURN_OFF_ALERT}
}