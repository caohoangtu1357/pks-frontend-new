import {
    GET_INFO_SUCCESS,
    CHANGE_PASSWORD_SUCCESS,
    UPDATE_FB_ID_SUCCESS,
    GET_INFO,
    UPDATE_FB_ID,
    CHANGE_PASSWORD
} from "@custom/constants/ActionTypes/User";

export function getInfoSuccess(data) {
    return { type: GET_INFO_SUCCESS, data: data }
}

export function getInfoFail() {
    return { type: GET_INFO_SUCCESS }
}

export function changePasswordSuccess() {
    return { type: CHANGE_PASSWORD_SUCCESS }
}

export function changePassword(passwordInfo) {
    return { type: CHANGE_PASSWORD, passwordInfo }
}

export function updateFbIdSuccess() {
    return { type: UPDATE_FB_ID_SUCCESS }
}

export function updateFbId(fbId) {
    return { type: UPDATE_FB_ID, fbId }
}

export function getInfo() {
    return { type: GET_INFO }
}