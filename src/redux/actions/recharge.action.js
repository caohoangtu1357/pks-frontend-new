import {
    GET_RECHARGE_SUCCESS,
    GET_RECHARGE,
    GET_RECHARGE_ALL,
    GET_RECHARGE_ALL_SUCCESS
} from "@custom/constants/ActionTypes/Recharge";

export function getRechargeSuccess(data) {
    return { type: GET_RECHARGE_SUCCESS, data: data }
}

export function getRecharge() {
    return { type: GET_RECHARGE }
}

export function getRechargeAll(filters) {
    return { type: GET_RECHARGE_ALL, filters }
}

export function getRechargeAllSuccess(data) {
    return { type: GET_RECHARGE_ALL_SUCCESS, data }
}