import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import { useDispatch } from 'react-redux';
import Button from '@material-ui/core/Button';
import { Box } from '@material-ui/core';
import ContentLoader from '../../ContentLoader';
import { alpha, makeStyles } from '@material-ui/core/styles';
import CmtImage from '../../../../@coremat/CmtImage';
import Typography from '@material-ui/core/Typography';
import { CurrentAuthMethod } from '../../../constants/AppConstants';
import { NavLink } from 'react-router-dom';
import AuthWrapper from './AuthWrapper';
import { verifyAccount } from 'redux/actions';
import { Alert } from '@material-ui/lab';
import { useForm } from 'react-hook-form';

const useStyles = makeStyles(theme => ({
    authThumb: {
        backgroundColor: alpha(theme.palette.primary.main, 0.12),
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '50%',
            order: 2,
        },
    },
    authContent: {
        padding: 30,
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: props => (props.variant === 'default' ? '50%' : '100%'),
            order: 1,
        },
        [theme.breakpoints.up('xl')]: {
            padding: 50,
        },
    },
    titleRoot: {
        marginBottom: 14,
        color: theme.palette.text.primary,
    },
    textFieldRoot: {
        '& .MuiOutlinedInput-notchedOutline': {
            borderColor: alpha(theme.palette.common.dark, 0.12),
        },
    },
    formcontrolLabelRoot: {
        '& .MuiFormControlLabel-label': {
            [theme.breakpoints.down('xs')]: {
                fontSize: 12,
            },
        },
    },
}));
//variant = 'default', 'standard'
const VerifyAccount = ({ method = CurrentAuthMethod, variant = 'default', wrapperVariant = 'default' }) => {
    const [phone, setPhone] = useState('');
    const [token, setToken] = useState('');
    const dispatch = useDispatch();
    const classes = useStyles({ variant });

    const { register, handleSubmit, formState: { errors }, reset } = useForm();

    const handleCreate = (data, e) => {
        dispatch(verifyAccount({ 
            phone: data.phone, 
            token: data.token 
        }));
    };

    return (
        <AuthWrapper variant={wrapperVariant}>
            {variant === 'default' ? (
                <Box className={classes.authThumb}>
                    <CmtImage src={'/images/auth/login-img.png'} />
                </Box>
            ) : null}
            <Box className={classes.authContent}>
                <Alert style={{ marginBottom: 20 }} variant="filled" severity="success">Hãy nhập token mà chúng tôi đã gửi cho bạn.</Alert>
                <Typography component="div" variant="h1" className={classes.titleRoot}>
                    Xác thực tài khoản
                </Typography>
                <form onSubmit={handleSubmit(handleCreate)}>
                    <Box mb={2}>
                        <TextField
                            {...register("phone", { required: true })}
                            size="small"
                            label="Số điện thoại"
                            fullWidth
                            margin="normal"
                            variant="outlined"
                            className={classes.textFieldRoot}
                        />
                        {errors.phone?.type == "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                    </Box>
                    <Box mb={2}>
                        <TextField
                            {...register("token", { required: true })}
                            size="small"
                            label="Mã xác thực"
                            fullWidth
                            margin="normal"
                            variant="outlined"
                            className={classes.textFieldRoot}
                        />
                        {errors.token?.type == "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
                    </Box>
                    <Box display="flex" alignItems="center" justifyContent="space-between" mb={5}>
                        <Button type="submit" variant="contained" color="primary">
                            Xác thực
                        </Button>

                        <Box component="p" fontSize={{ xs: 12, sm: 16 }}>
                            <NavLink to="/signin">
                                Đăng nhập với tài khoản khác
                            </NavLink>
                        </Box>
                    </Box>
                </form>
                <ContentLoader />
            </Box>
        </AuthWrapper>
    );
};

export default VerifyAccount;
