import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import IntlMessages from '../../../utils/IntlMessages';
import { useDispatch } from 'react-redux';
import Button from '@material-ui/core/Button';
import { Box } from '@material-ui/core';
import ContentLoader from '../../ContentLoader';
import { alpha, makeStyles } from '@material-ui/core/styles';
import CmtImage from '../../../../@coremat/CmtImage';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { CurrentAuthMethod } from '../../../constants/AppConstants';
import { NavLink } from 'react-router-dom';
import AuthWrapper from './AuthWrapper';
import { login } from 'redux/actions';
import { useForm } from 'react-hook-form';

const useStyles = makeStyles(theme => ({
  authThumb: {
    backgroundColor: alpha(theme.palette.primary.main, 0.12),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '50%',
      order: 2,
    },
  },
  authContent: {
    padding: 30,
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: props => (props.variant === 'default' ? '50%' : '100%'),
      order: 1,
    },
    [theme.breakpoints.up('xl')]: {
      padding: 50,
    },
  },
  titleRoot: {
    marginBottom: 14,
    color: theme.palette.text.primary,
  },
  textFieldRoot: {
    '& .MuiOutlinedInput-notchedOutline': {
      borderColor: alpha(theme.palette.common.dark, 0.12),
    },
  },
  formcontrolLabelRoot: {
    '& .MuiFormControlLabel-label': {
      [theme.breakpoints.down('xs')]: {
        fontSize: 12,
      },
    },
  },
}));
//variant = 'default', 'standard'
const SignIn = ({ method = CurrentAuthMethod, variant = 'default', wrapperVariant = 'default' }) => {

  const { register, handleSubmit, formState: { errors }, reset } = useForm();

  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useDispatch();
  const classes = useStyles({ variant });

  function handleCreate(data,e){
    dispatch(login({ 
      phone: data.phone, 
      password: data.password 
    }));
  }

  return (
    <AuthWrapper variant={wrapperVariant}>
      {variant === 'default' ? (
        <Box className={classes.authThumb}>
          <CmtImage src={'/images/auth/login-img.png'} />
        </Box>
      ) : null}
      <Box className={classes.authContent}>
        <Box mb={7}>
          <CmtImage src={'/images/logo.png'} />
        </Box>
        <Typography component="div" variant="h1" className={classes.titleRoot}>
          Đăng nhập
        </Typography>
        <form onSubmit={handleSubmit(handleCreate)}>
          <Box mb={2}>
            <TextField
              {...register('phone',{required:true})}
              size="small"
              label="Số điện thoại"
              fullWidth
              margin="normal"
              variant="outlined"
              className={classes.textFieldRoot}
            />
            {errors.phone?.type=="required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
          </Box>
          <Box mb={2}>
            <TextField
              {...register('password',{required:true})}
              size="small"
              type="password"
              label="Mật khẩu"
              fullWidth
              margin="normal"
              variant="outlined"
              className={classes.textFieldRoot}
            />
            {errors.password?.type=="required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
          </Box>
          <Box display="flex" alignItems="center" justifyContent="space-between" mb={5}>
            <FormControlLabel
              className={classes.formcontrolLabelRoot}
              control={<Checkbox name="checkedA" />}
              label="Ghi nhớ"
            />
            <Box component="p" fontSize={{ xs: 12, sm: 16 }}>
              <NavLink to="/forgot-password">
                Quên mật khẩu ?
              </NavLink>
            </Box>
          </Box>

          <Box display="flex" alignItems="center" justifyContent="space-between" mb={5}>
            <Button type="submit" variant="contained" color="primary">
              Đăng nhập
            </Button>

            <Box component="p" fontSize={{ xs: 12, sm: 16 }}>
              <NavLink to="/signup">
                Đăng kí tài khoản
              </NavLink>
            </Box>
          </Box>
        </form>

        {/* {dispatch(AuhMethods[method].getSocialMediaIcons())} */}

        <ContentLoader />
      </Box>
    </AuthWrapper>
  );
};

export default SignIn;
