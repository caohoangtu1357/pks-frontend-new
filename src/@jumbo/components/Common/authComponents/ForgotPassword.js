import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Collapse, IconButton, Link } from '@material-ui/core';
import { alpha } from '@material-ui/core/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Typography from '@material-ui/core/Typography';
import { Alert } from '@material-ui/lab';
import CloseIcon from '@material-ui/icons/Close';

import CmtImage from '../../../../@coremat/CmtImage';

import IntlMessages from '../../../utils/IntlMessages';
// import { AuhMethods } from '../../../../services/auth';
import ContentLoader from '../../ContentLoader';
import { CurrentAuthMethod } from '../../../constants/AppConstants';
import AuthWrapper from './AuthWrapper';
import { resetPassword } from '../../../../redux/actions';
import { useForm } from 'react-hook-form';

const useStyles = makeStyles(theme => ({
  authThumb: {
    backgroundColor: alpha(theme.palette.primary.main, 0.12),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    [theme.breakpoints.up('md')]: {
      width: '50%',
      order: 2,
    },
  },
  authContent: {
    padding: 30,
    [theme.breakpoints.up('md')]: {
      order: 1,
      width: props => (props.variant === 'default' ? '50%' : '100%'),
    },
    [theme.breakpoints.up('xl')]: {
      padding: 50,
    },
  },
  titleRoot: {
    marginBottom: 14,
    color: theme.palette.text.primary,
  },
  textFieldRoot: {
    '& .MuiOutlinedInput-notchedOutline': {
      borderColor: alpha(theme.palette.common.dark, 0.12),
    },
  },
  alertRoot: {
    marginBottom: 10,
  },
}));

//variant = 'default', 'standard', 'bgColor'
const ForgotPassword = ({ method = CurrentAuthMethod, variant = 'default', wrapperVariant = 'default' }) => {
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();
  const classes = useStyles({ variant });

  const { register, handleSubmit, formState: { errors }, reset } = useForm();

  const handleCreate = (data, e) => {
    dispatch(resetPassword({
      new_password: data.newPassword,
      phone: data.phone,
      confirm_password: data.confirmPassword
    }));
  };

  return (
    <AuthWrapper variant={wrapperVariant}>
      {variant === 'default' ? (
        <div className={classes.authThumb}>
          <CmtImage src={'/images/auth/forgot-img.png'} />
        </div>
      ) : null}
      <div className={classes.authContent}>
        <div className={'mb-7'}>
          <CmtImage src={'/images/logo.png'} />
        </div>
        <Typography component="div" variant="h1" className={classes.titleRoot}>
          Quên mật khẩu
        </Typography>
        <Collapse in={open}>
          <Alert
            variant="outlined"
            severity="success"
            className={classes.alertRoot}
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setOpen(false);
                }}>
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }>
            A mail has been sent on your email address with reset password link.
          </Alert>
        </Collapse>
        <form onSubmit={handleSubmit(handleCreate)}>
          <div>
            <TextField
              {...register('phone', { required: true })}
              size="small"
              label="Số điện thoại"
              fullWidth
              margin="normal"
              variant="outlined"
              className={classes.textFieldRoot}
            />
            {errors.phone?.type == "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
          </div>
          <div>
            <TextField
              {...register('newPassword', { required: true })}
              type="password"
              size="small"
              label="Mật khẩu mới"
              fullWidth
              margin="normal"
              variant="outlined"
              className={classes.textFieldRoot}
            />
            {errors.newPassword?.type == "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
          </div>
          <div className={'mb-5'}>
            <TextField
              {...register('confirmPassword', { required: true })}
              type="password"
              size="small"
              label="Nhập lại mật khẩu"
              fullWidth
              margin="normal"
              variant="outlined"
              className={classes.textFieldRoot}
            />
            {errors.confirmPassword?.type == "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
          </div>
          <div className={'mb-5'}>
            <Button type="submit" variant="contained" color="primary">
              Đặt lại mật khẩu
            </Button>
          </div>

          <div>
            <Typography>
              <span className={'ml-2'}>
                <Link href="/signin">
                  Đăng nhập tài khoản khác
                </Link>
              </span>
            </Typography>
          </div>
        </form>
        <ContentLoader />
      </div>
    </AuthWrapper>
  );
};

export default ForgotPassword;
