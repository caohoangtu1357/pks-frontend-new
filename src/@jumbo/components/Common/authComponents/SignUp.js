import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Box, Grid } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import ContentLoader from '../../ContentLoader';
import { alpha, makeStyles } from '@material-ui/core/styles';
import CmtImage from '../../../../@coremat/CmtImage';
import Typography from '@material-ui/core/Typography';
import { CurrentAuthMethod } from '../../../constants/AppConstants';
import AuthWrapper from './AuthWrapper';
import { NavLink } from 'react-router-dom';
import GridContainer from '@jumbo/components/GridContainer';
import { register as registerAction } from "../../../../redux/actions";
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { useForm } from 'react-hook-form';
import { validateEmail, validatePhone, validateFacebookProfile } from '@custom/config/General'

const useStyles = makeStyles(theme => ({
  authThumb: {
    backgroundColor: alpha(theme.palette.primary.main, 0.12),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    [theme.breakpoints.up('md')]: {
      width: '50%',
      order: 2,
    },
  },
  authContent: {
    padding: 30,
    [theme.breakpoints.up('md')]: {
      width: '100%',
      order: 1,
    },
    [theme.breakpoints.up('xl')]: {
      padding: 50,
    },
  },
  titleRoot: {
    marginBottom: 14,
    color: theme.palette.text.primary,
  },
  textFieldRoot: {
    '& .MuiOutlinedInput-notchedOutline': {
      borderColor: alpha(theme.palette.common.dark, 0.12),
    },
  },
  textCapital: {
    textTransform: 'capitalize',
  },
  textAcc: {
    textAlign: 'center',
    '& a': {
      marginLeft: 4,
      marginTop: 20
    },
  },
  alrTextRoot: {
    textAlign: 'center',
    [theme.breakpoints.up('sm')]: {
      textAlign: 'right',
    },
  },
}));

//variant = 'default', 'standard', 'bgColor'
const SignUp = ({ method = CurrentAuthMethod, variant = 'default', wrapperVariant = 'default' }) => {
  const [isAccepted, setIsAccepted] = useState(true);
  const dispatch = useDispatch();
  const classes = useStyles({ variant });
  const { register, handleSubmit, formState: { errors }, reset } = useForm();

  function handleCreate(data, e) {

    if (!validatePhone(data.phone) || !validateEmail(data.email) || !validateFacebookProfile(data.link)) {
      return false;
    }

    dispatch(registerAction({
      full_name: data.name,
      email: data.email,
      phone: data.phone,
      facebook_link: data.link,
      password: data.password,
      password_confirm: data.passwordConfirm,
      is_accepted: isAccepted ? 1 : ""
    }));
  };

  return (
    <AuthWrapper variant={wrapperVariant}>
      <Box className={classes.authContent}>
        <Box mb={7}>
          <CmtImage src={'/images/logo.png'} />
        </Box>
        <Typography component="div" variant="h1" className={classes.titleRoot}>
          Đăng kí tài khoản
        </Typography>
        <form onSubmit={handleSubmit(handleCreate)}>
          <GridContainer>
            <Grid item sm={6}>
              <Box mb={2}>
                <TextField
                  {...register("name", { required: true })}
                  size="small"
                  label="Họ và tên"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  className={classes.textFieldRoot}
                />
                {errors.name?.type == "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
              </Box>
              <Box mb={2}>
                <TextField
                  {...register("email", { required: true })}
                  size="small"
                  type="email"
                  label="Email"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  className={classes.textFieldRoot}
                />
                {errors.email?.type == "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
              </Box>
              <Box mb={2}>
                <TextField
                  {...register("phone", { required: true })}
                  size="small"
                  label="Số điện thoại"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  className={classes.textFieldRoot}
                />
                {errors.phone?.type == "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
              </Box>
            </Grid>
            <Grid item sm={6}>
              <Box mb={2}>
                <TextField
                  {...register("link", { required: true })}
                  size="small"
                  label="Link facebook"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  className={classes.textFieldRoot}
                />
                {errors.link?.type == "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
              </Box>
              <Box mb={2}>
                <TextField
                  {...register("password", { required: true })}
                  size="small"
                  type="password"
                  label="Mật khẩu"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  className={classes.textFieldRoot}
                />
                {errors.password?.type == "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
              </Box>
              <Box mb={2}>
                <TextField
                  {...register("passwordConfirm", { required: true })}
                  size="small"
                  type="password"
                  label="Nhập lại mật khẩu"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  className={classes.textFieldRoot}
                />
                {errors.passwordConfirm?.type == "required" && <p className="form-validate-error">Phần thông tin bắt buộc.</p>}
              </Box>
            </Grid>
          </GridContainer>
          <Box
            display="flex"
            flexDirection={{ xs: 'column', sm: 'row' }}
            alignItems={{ sm: 'center' }}
            justifyContent={{ sm: 'space-between' }}
            mb={3}>
            <FormGroup>
              <FormControlLabel control={<Checkbox checked={isAccepted}
                onChange={e => {
                  setIsAccepted(e.target.checked);
                }} />} label="Tôi đồng ý với Điều khoản và dịch vụ." />
            </FormGroup>
            <Box mb={{ xs: 2, sm: 0 }}>
              <Button type="submit" variant="contained" color="primary">
                Đăng kí
              </Button>
            </Box>
          </Box>
        </form>
        <Typography className={classes.textAcc}>
          Bạn đã có tài khoản?
          <NavLink to="/signin">Đăng nhập</NavLink>
        </Typography>
        <ContentLoader />
      </Box>
    </AuthWrapper>
  );
};

export default SignUp;
