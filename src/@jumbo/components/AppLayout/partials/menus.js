import React from 'react';
import { DonutSmall, Help, Home, LiveTvSharp, ModeComment, MonetizationOn, PeopleAltSharp, PostAdd, ThumbUp } from '@material-ui/icons';
import IntlMessages from '../../../utils/IntlMessages';
import Share from '@material-ui/icons/Share';

const systemMenu = [
  {
    name: "Trang chủ",
    icon: <Home/>,
    type: 'item',
    link: '/system/',
  },
  {
    name: "Nạp tiền",
    icon: <MonetizationOn />,
    type: 'item',
    link: '/system/recharge',
  },
  {
    name: "Liên hệ hỗ trợ",
    icon: <Help />,
    type: 'item',
    link: '/system/contact',
  },
  {
    name: "Lịch sử giao dịch",
    icon: <DonutSmall />,
    type: 'item',
    link: '/system/order',
  },
  {
    name: "Cài đặt tài khoản",
    icon: <PeopleAltSharp />,
    type: 'item',
    link: '/system/setting',
  }
];

const facebookMenu = [
  {
    name: "Buff Like bài viết",
    icon: <ThumbUp/>,
    type: 'item',
    link: '/facebook/buff-like',
  },
  {
    name: "Buff Cmt bài viết",
    icon: <ModeComment />,
    type: 'item',
    link: '/facebook/buff-comment',
  },
  {
    name: "Buff Share bài viết",
    icon: <Share />,
    type: 'item',
    link: '/facebook/buff-share',
  },
  {
    name: "Buff Livestream",
    icon: <LiveTvSharp />,
    type: 'item',
    link: '/facebook/buff-livestream',
  }
];

export const sidebarNavs = [
  {
    name: 'Hệ Thống',
    type: 'section',
    children: systemMenu,
  },
  {
    name: 'Facebook',
    type: 'section',
    children: facebookMenu,
  }
];

export const horizontalDefaultNavs = [
  {
    name: <IntlMessages id={'sidebar.main'} />,
    type: 'collapse',
    children: [
      
    ],
  },
];

export const minimalHorizontalMenus = [
  {
    name: <IntlMessages id={'sidebar.main'} />,
    type: 'collapse',
    children: [
      
    ],
  },
];
